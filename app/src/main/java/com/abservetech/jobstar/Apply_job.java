package com.abservetech.jobstar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 9/21/2016.
 */
public class Apply_job extends AppCompatActivity {

    public static String count;
    ImageView back;

    ListView recommended_listView;
    TextView applied_count,name,email,ph_no,dob,age,qualification,marital_status,address,experience;
    String username1,email1,phno1,date_of_birth,experience1,gender,pincode,marital_status1,permanent_address;
    ImageView profile_edit,cv,cover_letter;
    public  static List<HomepageGS> movieList = new ArrayList<HomepageGS>();
    PrefManager prefManager;
    ProgressDialog pd;

    private static final int RESULT_LOAD_IMAGE = 1;

    Button send, search;
    String path, selectedFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_apply);
        pd = new ProgressDialog(Apply_job.this);
        prefManager = new PrefManager(this);
        applied_count=(TextView)findViewById(R.id.applied_count);
        name=(TextView)findViewById(R.id.name);
        email=(TextView)findViewById(R.id.email);
        ph_no=(TextView)findViewById(R.id.ph_no);
        dob=(TextView)findViewById(R.id.dob);
        age=(TextView)findViewById(R.id.age);
        address=(TextView)findViewById(R.id.address);
        experience=(TextView)findViewById(R.id.experience);
        qualification=(TextView)findViewById(R.id.qualification);
        marital_status=(TextView)findViewById(R.id.marital_status);
        profile_edit=(ImageView)findViewById(R.id.profile_edit);
        cv=(ImageView)findViewById(R.id.cv);
        cover_letter=(ImageView)findViewById(R.id.cover_letter);

        cover_letter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Apply_job.this, R.style.AppCompatAlertDialogStyle);

                builder.setTitle("Choose your option");
                builder.setCancelable(false);
                builder.setItems(new CharSequence[]
                                {"Write", "Attach", "Cancel"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item
                                switch (which) {
                                    case 0:
                                            Intent i= new Intent(Apply_job.this,Coverletter.class);
                                            startActivity(i);
                                        break;
                                    case 1:
                                        showFileChooser();
                                        new UploadFileAsync().execute(selectedFilePath);
                                        break;
                                    case 2:
                                        dialog.dismiss();
                                        break;
                                }
                            }
                        });

                final android.support.v7.app.AlertDialog alert = builder.create();
                alert.show();
            }
        });

        profile_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(Apply_job.this,Profile.class);
                startActivity(i);
            }
        });
        new Myprofileshow().execute();
    }

    class Myprofileshow extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "allprofiles";
            HttpPost httppost = new HttpPost(url);
            String group_id = "4";
            Intent i = getIntent();

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id", prefManager.getuserid()));

                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());

         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);

                JSONArray jsonArray = jsonRootObject.optJSONArray("Profile_details");

                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {



                        JSONObject jsonObject = jsonArray.getJSONObject(i1);

                        String user_id = jsonObject.getString("user_id").toString();
                        username1 = jsonObject.getString("username").toString();
                        email1 = jsonObject.getString("email").toString();
                        phno1 = jsonObject.getString("phone_number").toString();
                        date_of_birth = jsonObject.getString("date_of_birth").toString();
                        experience1 = jsonObject.getString("experience").toString();
                        gender = jsonObject.getString("gender").toString();
                        pincode = jsonObject.getString("pincode").toString();
                        marital_status1 = jsonObject.getString("marital_status").toString();
                        permanent_address = jsonObject.getString("permanent_address").toString();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            name.setText(username1);
                            email.setText(email1);
                            ph_no.setText(phno1);
                            experience.setText(experience1);
                            dob.setText(date_of_birth);
                            marital_status.setText(marital_status1);
                            address.setText(permanent_address);


                        }
                    });
                }

            }
          catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.apply, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected



            case R.id.apply_menu:

                break;
            // action with ID action_settings was selected

            default:
                break;
        }

        return true;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                if (data == null) {
                    //no data present
                    return;
                }


                Uri selectedFileUri = data.getData();
                selectedFilePath = FilePath.getPath(this, selectedFileUri);
                Log.d("Selected File Path:", selectedFilePath);

                if (selectedFilePath != null && !selectedFilePath.equals("")) {
                    //textView.setText(selectedFilePath);
                } else {
                    Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    class UploadFileAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {

                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                File sourceFile = new File(selectedFilePath);

                if (sourceFile.isFile()) {

                    try {

                        String upLoadServerUri = Serviceurl.url + "cvinsert";
                        // open a URL connection to the Servlet
                        FileInputStream fileInputStream = new FileInputStream(
                                sourceFile);
                        URL url = new URL(upLoadServerUri);


                        conn = (HttpURLConnection) url.openConnection();
                        conn.setDoInput(true);
                        conn.setDoOutput(true);
                        conn.setUseCaches(false);
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Connection", "Keep-Alive");
                        conn.setRequestProperty("ENCTYPE",
                                "multipart/form-data");
                        conn.setRequestProperty("Content-Type",
                                "multipart/form-data;boundary=" + boundary);

                        conn.setRequestProperty("cvfile", selectedFilePath);


                        dos = new DataOutputStream(conn.getOutputStream());

                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"cvfile\";filename=\""
                                + selectedFilePath + "\"" + lineEnd);

                        dos.writeBytes(lineEnd);

                        // create a buffer of maximum size
                        bytesAvailable = fileInputStream.available();

                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];

                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        while (bytesRead > 0) {

                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math
                                    .min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0,
                                    bufferSize);

                        }

                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens
                                + lineEnd);

                        // Responses from the server (code and message)
                        int serverResponseCode = conn.getResponseCode();
                        String serverResponseMessage = conn
                                .getResponseMessage();

                        Log.e("serverResponseMessage", serverResponseMessage);



                        // close the streams //
                        fileInputStream.close();
                        dos.flush();
                        dos.close();

                    } catch (Exception e) {

                        // dialog.dismiss();
                        e.printStackTrace();

                    }
                    // dialog.dismiss();

                } // End else block


            } catch (Exception ex) {
                // dialog.dismiss();

                ex.printStackTrace();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}