package com.abservetech.jobstar;

/**
 * Created by ABSERVETECH on 10-Sep-16.
 */
public class ProfileGS {
    String employment_designation;
    String organization;
    String current_company;
    String started_worked_from;
    String worked_till;
    String describe_job_profile;
    String notice_period;
    String skills_software;
    String version;
    String last_used;
    String total_experience;
    String total_months;
    String project_title, projectclient, started_working_from, worked_tills, details_project, project_location, project_site, nature_employment, team_size, role, role_description, skills_used;
    String industry_type, department, roles, desired_job_type, desired_employment_type, preferred_location, available_to_join, work_permit_usa, work_permit, date_of_birth, gender, home_town, pincode, marital_status, permanent_address;
    String education, course, specialization, institute, year_of_passing, time_periods;
    String languages, lan_proficiency, read, write,emp_id, speak,project_id,skills_id,edu_id,lang_id,profile_id,keyskillname,keyskillid;

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getLan_proficiency() {
        return lan_proficiency;
    }

    public void setLan_proficiency(String lan_proficiency) {
        this.lan_proficiency = lan_proficiency;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getWrite() {
        return write;
    }

    public void setWrite(String write) {
        this.write = write;
    }

    public String getSpeak() {
        return speak;
    }

    public void setSpeak(String speak) {
        this.speak = speak;
    }

    public String getIndustry_type() {
        return industry_type;
    }

    public void setIndustry_type(String industry_type) {
        this.industry_type = industry_type;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getDesired_job_type() {
        return desired_job_type;
    }

    public void setDesired_job_type(String desired_job_type) {
        this.desired_job_type = desired_job_type;
    }

    public String getDesired_employment_type() {
        return desired_employment_type;
    }

    public void setDesired_employment_type(String desired_employment_type) {
        this.desired_employment_type = desired_employment_type;
    }

    public String getPreferred_location() {
        return preferred_location;
    }

    public void setPreferred_location(String preferred_location) {
        this.preferred_location = preferred_location;
    }

    public String getAvailable_to_join() {
        return available_to_join;
    }

    public void setAvailable_to_join(String available_to_join) {
        this.available_to_join = available_to_join;
    }

    public String getWork_permit_usa() {
        return work_permit_usa;
    }

    public void setWork_permit_usa(String work_permit_usa) {
        this.work_permit_usa = work_permit_usa;
    }

    public String getWork_permit() {
        return work_permit;
    }

    public void setWork_permit(String work_permit) {
        this.work_permit = work_permit;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHome_town() {
        return home_town;
    }

    public void setHome_town(String home_town) {
        this.home_town = home_town;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getPermanent_address() {
        return permanent_address;
    }

    public void setPermanent_address(String permanent_address) {
        this.permanent_address = permanent_address;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getYear_of_passing() {
        return year_of_passing;
    }

    public void setYear_of_passing(String year_of_passing) {
        this.year_of_passing = year_of_passing;
    }

    public String getTime_periods() {
        return time_periods;
    }

    public void setTime_periods(String time_periods) {
        this.time_periods = time_periods;
    }

    public String getProject_title() {
        return project_title;
    }

    public void setProject_title(String project_title) {
        this.project_title = project_title;
    }

    public String getProjectclient() {
        return projectclient;
    }

    public void setProjectclient(String projectclient) {
        this.projectclient = projectclient;
    }

    public String getStarted_working_from() {
        return started_working_from;
    }

    public void setStarted_working_from(String started_working_from) {
        this.started_working_from = started_working_from;
    }

    public String getWorked_tills() {
        return worked_tills;
    }

    public void setWorked_tills(String worked_tills) {
        this.worked_tills = worked_tills;
    }

    public String getDetails_project() {
        return details_project;
    }

    public void setDetails_project(String details_project) {
        this.details_project = details_project;
    }

    public String getProject_location() {
        return project_location;
    }

    public void setProject_location(String project_location) {
        this.project_location = project_location;
    }

    public String getProject_site() {
        return project_site;
    }

    public void setProject_site(String project_site) {
        this.project_site = project_site;
    }

    public String getNature_employment() {
        return nature_employment;
    }

    public void setNature_employment(String nature_employment) {
        this.nature_employment = nature_employment;
    }

    public String getTeam_size() {
        return team_size;
    }

    public void setTeam_size(String team_size) {
        this.team_size = team_size;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole_description() {
        return role_description;
    }

    public void setRole_description(String role_description) {
        this.role_description = role_description;
    }

    public String getSkills_used() {
        return skills_used;
    }

    public void setSkills_used(String skills_used) {
        this.skills_used = skills_used;
    }

    public String getSkills_software() {
        return skills_software;
    }

    public void setSkills_software(String skills_software) {
        this.skills_software = skills_software;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLast_used() {
        return last_used;
    }

    public void setLast_used(String last_used) {
        this.last_used = last_used;
    }

    public String getTotal_experience() {
        return total_experience;
    }

    public void setTotal_experience(String total_experience) {
        this.total_experience = total_experience;
    }

    public String getTotal_months() {
        return total_months;
    }

    public void setTotal_months(String total_months) {
        this.total_months = total_months;
    }

    public String getNotice_period() {
        return notice_period;
    }

    public void setNotice_period(String notice_period) {
        this.notice_period = notice_period;
    }

    public String getEmployment_designation() {
        return employment_designation;
    }

    public void setEmployment_designation(String employment_designation) {
        this.employment_designation = employment_designation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getCurrent_company() {
        return current_company;
    }

    public void setCurrent_company(String current_company) {
        this.current_company = current_company;
    }

    public String getStarted_worked_from() {
        return started_worked_from;
    }

    public void setStarted_worked_from(String started_worked_from) {
        this.started_worked_from = started_worked_from;
    }

    public String getWorked_till() {
        return worked_till;
    }

    public void setWorked_till(String worked_till) {
        this.worked_till = worked_till;
    }

    public String getDescribe_job_profile() {
        return describe_job_profile;
    }

    public void setDescribe_job_profile(String describe_job_profile) {
        this.describe_job_profile = describe_job_profile;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setSkills_id(String skills_id) {
        this.skills_id = skills_id;
    }

    public String getSkills_id() {
        return skills_id;
    }

    public void setEdu_id(String edu_id) {
        this.edu_id = edu_id;
    }

    public String getEdu_id() {
        return edu_id;
    }

    public void setLang_id(String lang_id) {
        this.lang_id = lang_id;
    }

    public String getLang_id() {
        return lang_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setKeyskillname(String keyskillname) {
        this.keyskillname = keyskillname;
    }

    public void setKeyskillid(String keyskillid) {
        this.keyskillid = keyskillid;
    }

    public String getKeyskillname() {
        return keyskillname;
    }

    public String getKeyskillid() {
        return keyskillid;
    }
}
