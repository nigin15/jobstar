package com.abservetech.jobstar;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainSearchPage  extends Activity {

    EditText search;
    ListView search_list;
    SearchRestaurantadapter adapter;
    TextView emptyview;
    public String jobsearch;
    private List<MainPageGS> mainpagelist = new ArrayList<MainPageGS>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_main_search_page);
        search=(EditText)findViewById(R.id.searchmain);
        emptyview=(TextView)findViewById(R.id.text);
        search_list=(ListView)findViewById(R.id.search_list);
        adapter = new SearchRestaurantadapter(MainSearchPage.this, mainpagelist);


        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, final int count) {
                jobsearch = s.toString();
                Log.d("log", "result count:" + jobsearch);

                new AsyncT().execute(jobsearch);
                adapter.getFilter().filter(s);
                adapter.getFilter().filter(s, new Filter.FilterListener() {
                    public void onFilterComplete(int count) {
                        Log.d("log", "result count:" + count);
                        if (count == 0) {
                            emptyview.setText("0 results found");
                            search_list.setVisibility(View.GONE);
                            search.setVisibility(View.VISIBLE);
                        } else {
                            emptyview.setText(count + "results found");
                            search_list.setVisibility(View.VISIBLE);
                        }
                        search.setVisibility(View.VISIBLE);
                    }
                });


            }


            @Override
            public void afterTextChanged(Editable s) {


            }
        });

    }

    class AsyncT extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            // http://abservetech.com/demoprojects/android/foodstar/public/mobile/restaurant/restaurantresults

            HttpClient httpclient = new DefaultHttpClient();
            String url =Serviceurl.url + "seekersearch";
            HttpPost httppost = new HttpPost(url);

            try {
                // Add your data

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("search_text", "company"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                JSONArray jsonArray = jsonRootObject.optJSONArray("Recent_Posts");
                mainpagelist.clear();

                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {


                        JSONObject jsonObject = jsonArray.getJSONObject(i1);

                        String id = jsonObject.optString("id").toString();
                        String user_id = jsonObject.optString("user_id").toString();
                        String job_title = jsonObject.optString("job_title").toString();
                        String job_desc = jsonObject.optString("job_desc").toString();
                        String  created = jsonObject.optString("created").toString();

                        Log.d("response", "profileresponse -----" + id + user_id + job_title + job_desc + created);

                        MainPageGS mainPageGS = new MainPageGS();

                        mainPageGS.setId(id);
                        mainPageGS.setUser_id(user_id);
                        mainPageGS.setJob_title(job_title);
                        mainPageGS.setJob_desc(job_desc);
                        mainPageGS.setCreated(created);

                        mainpagelist.add(mainPageGS);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                search_list.setAdapter(adapter);

                                adapter.notifyDataSetChanged();
                            }});
                    }
                    catch (JSONException e) {

                        e.printStackTrace();
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);


        }
    }


    class SearchRestaurantadapter extends BaseAdapter {


        private List<MainPageGS> movieItems;
        public TextView restaurant_name,amount,details,rating,clock_time,opn_time,food_item;
        public ImageView image1,rupee1,rupee2,rupee3,rupee4,rupee11,rupee12,rupee13,rupee14,clock;
        ValueFilter valueFilter;
        Context mcontext;
        private List<MainPageGS> mStringFilterList;
        String opening[] = new String[50];
        public SearchRestaurantadapter(Context mcontext, List<MainPageGS> movieItems) {

            this.mcontext = mcontext;
            this.movieItems = movieItems;
            this.mStringFilterList = movieItems;
            this.mStringFilterList.addAll(movieItems);
        }

        @Override
        public int getCount() {
            return movieItems.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                convertView = inflater.inflate(R.layout.mainlist, parent, false);
//                LayoutInflater mInflater = (LayoutInflater) mcontext
//                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//                convertView = mInflater.inflate(R.layout.sosfriendlist, null);

                holder = new ViewHolder();
                holder.jobtitle= (TextView) convertView.findViewById(R.id.jobtitle);
                holder.jobdesc= (TextView) convertView.findViewById(R.id.jobdesc);
                holder.jobtime= (TextView) convertView.findViewById(R.id.jobtime);
                convertView.setTag(holder);


                final MainPageGS m = mainpagelist.get(position);





                if (m.getJob_title() != null) {
                    if (!m.getJob_title().equals("null")) {

                        holder.jobtitle.setText(m.getJob_title());
                    }
                }

                if (m.getJob_desc() != null) {
                    if (!m.getJob_desc().equals("null")) {

                        holder.jobdesc.setText(m.getJob_desc());
                    }
                }
                if (m.getCreated()!= null) {
                    if (!m.getCreated().equals("null")) {

                        holder.jobtime.setText(m.getCreated());
                    }
                }


            } else {
                holder = (ViewHolder) convertView.getTag();
            }



            return convertView;
        }
         class ViewHolder {

            TextView jobtitle, jobdesc, jobtime;

        }
        public Filter getFilter() {
            if (valueFilter == null) {
                valueFilter = new ValueFilter();
            }
            return valueFilter;
        }

        private class ValueFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                List<MainPageGS> filterList = new ArrayList<MainPageGS>();
                if (constraint != null && constraint.length() > 0) {

                    for (int i = 0; i < mStringFilterList.size(); i++) {
//                        if ((mStringFilterList.get(i).getres_name1().toUpperCase())
//                                .contains(constraint.toString().toUpperCase())||(mStringFilterList.get(i).getres_fooditem().toUpperCase())
//                                .contains(constraint.toString().toUpperCase())) {
//
//                            MainPageGS m = mStringFilterList.get(i);
//                            filterList.add(m);
//                        }
//                    else if ((mStringFilterList.get(i).getres_name1().toUpperCase())
//                            .contains(constraint.toString().toUpperCase())) {
//
//                        Movie m = mStringFilterList.get(i);
//                        filterList.add(m);
//                    }
                    }
                    results.count = filterList.size();
                    results.values = filterList;
                } else {
                    results.count = filterList.size();
                    results.values = filterList;
                }
                return results;
            }



            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

            }


        }



    }


}

