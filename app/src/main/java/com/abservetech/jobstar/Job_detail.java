package com.abservetech.jobstar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/17/2016.
 */
public class Job_detail extends AppCompatActivity {

    public static String id;
    public static String fromsaved;
    public static String saved;
    public static String applied;
    ImageView back;
    ProgressDialog pd;
    PrefManager prefManager;
    Boolean favorite=false;
    TextView job_type,job_title,company_name,experience,location,pay_scale,date,
            description,industry,functional_area,role,key_skill,category,qualification,website,city,state,company_name2,address,country;

    String job_type1,job_title1,company_name1,experience1,location1,pay_scale1,date1,
            description1,industry1,functional_area1,role1,key_skill1,category1,qualification1,website1,city1,state1,country1,address1;
   String status;
    Button apply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_detailpage);
        prefManager = new PrefManager(this);
        pd = new ProgressDialog(Job_detail.this);
        job_title=(TextView)findViewById(R.id.job_title);
        company_name=(TextView)findViewById(R.id.company_name);
        experience=(TextView)findViewById(R.id.experience);
        location=(TextView)findViewById(R.id.location);
        pay_scale=(TextView)findViewById(R.id.pay_scale);
        date=(TextView)findViewById(R.id.date);
        description=(TextView)findViewById(R.id.description);
        industry=(TextView)findViewById(R.id.industry);
        functional_area=(TextView)findViewById(R.id.functional_area);
        role=(TextView)findViewById(R.id.role);
        key_skill=(TextView)findViewById(R.id.key_skill);
        category=(TextView)findViewById(R.id.category);
        qualification=(TextView)findViewById(R.id.qualification);
        company_name2=(TextView)findViewById(R.id.company_name2);
        website=(TextView)findViewById(R.id.website);
        country=(TextView)findViewById(R.id.country);
        state=(TextView)findViewById(R.id.state);
        address=(TextView)findViewById(R.id.address);
        apply=(Button)findViewById(R.id.apply);
        job_type=(TextView)findViewById(R.id.job_type);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i =new Intent(Job_detail.this,Apply_job.class);
                startActivity(i);
             //   new apply_async().execute();
            }
        });
        new Detail_async().execute();


    }

    class Detail_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "details";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id",id));

                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "Detailshow -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);



                JSONArray jsonArray4 = jsonRootObject.optJSONArray("Job_details");
                final String status = jsonRootObject.optString("status");


                for (int i1 = 0; i1 < jsonArray4.length(); i1++) {
                    try {



                        JSONObject jsonObject = jsonArray4.getJSONObject(i1);

                        job_title1 = (jsonObject.getString("job_title").toString());
                        company_name1 = (jsonObject.getString("company_name").toString());
                        experience1 = (jsonObject.getString("job_experience").toString());
                        pay_scale1= (jsonObject.getString("base_salary").toString());
                        date1 = (jsonObject.getString("date").toString());
                        description1 = (jsonObject.getString("job_description").toString());
                        location1 = (jsonObject.getString("job_location").toString());
                        industry1 = (jsonObject.getString("industry").toString());
                        functional_area1 = (jsonObject.getString("functional_area").toString());
                        key_skill1 = (jsonObject.getString("keyskills").toString());
                        role1 = (jsonObject.getString("role").toString());
                        category1 = (jsonObject.getString("role_category").toString());
                        qualification1 = (jsonObject.getString("min_qualification").toString());
                        job_type1= (jsonObject.getString("job_type").toString());
                        website1 = (jsonObject.getString("company_website").toString());
                        address1 = (jsonObject.getString("address").toString());
                        state1 = (jsonObject.getString("job_state").toString());
                        country1 = (jsonObject.getString("job_country").toString());



                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(status.equals("Already saved")){
                                    favorite=true;
                                   invalidateOptionsMenu();
                                }

                                job_title.setText(job_title1);
                                company_name.setText(company_name1);
                                experience.setText(experience1);
                                location.setText(location1);
                                pay_scale.setText(pay_scale1);
                                date.setText(date1);
                                description.setText(description1);
                                industry.setText(industry1);
                                job_type.setText(job_type1);
                                functional_area.setText(functional_area1);
                                role.setText(role1);
                                key_skill.setText(key_skill1);
                                category.setText(category1);

                                qualification.setText(qualification1);
                                website.setText(website1);

                                state.setText(state1);


                                company_name2.setText(company_name1);
                                address.setText(address1);
                                country.setText(country1);
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_save, menu);


        return true;
    }


    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.action_refresh1);
        if(saved!=null){
            if(saved.equals("True")){
                favorite = true;
                register.setIcon(R.drawable.like_fill);

            }
            else{
                register.setIcon(R.drawable.like);
                favorite = false;
            }

        Log.d("favorite--",""+favorite);}
        if(fromsaved!=null)
        {
            register.setVisible(false);
        }
        else
        {
            register.setVisible(true);
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                String text="www.jobportal.com/job-listings-urgent openings for"+job_title1+"-"+company_name1+"-"+location1+"-"+experience1+" year experience required";
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>"+text+"</p>"));
                startActivity(Intent.createChooser(sharingIntent,"Share using"));

                return true;

            case R.id.action_refresh1:
                if(favorite){
                    item.setIcon(R.drawable.like);
                    new deletesave_async().execute();
                    favorite = false;
                } else {
                    item.setIcon(R.drawable.like_fill);
                    new save_async().execute();
                    favorite = true;
                }
                return true;
            case android.R.id.home:
               finish();
                return true;
            default:
                break;
        }

        return true;
    }

    class save_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url1 + "save";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("post_id",id));
                nameValuePairs.add(new BasicNameValuePair("user_id", "27"));
                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "Detailshow -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);



                final String message = jsonRootObject.optString("message");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(Job_detail.this,message,Toast.LENGTH_SHORT).show();



                    }
                });



            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class deletesave_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url1 + "saveddelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("post_id",id));
                nameValuePairs.add(new BasicNameValuePair("user_id", "27"));
                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "Detailshow -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);



                final String message = jsonRootObject.optString("message");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(Job_detail.this,message,Toast.LENGTH_SHORT).show();



                    }
                });



            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class apply_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url1 + "applyjob";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("job_id",id));
                nameValuePairs.add(new BasicNameValuePair("user_id", "27"));
                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "Detailshow -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);



                final String message = jsonRootObject.optString("message");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(Job_detail.this,message,Toast.LENGTH_SHORT).show();

                    }
                });



            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
}