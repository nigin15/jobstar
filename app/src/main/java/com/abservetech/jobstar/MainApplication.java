package com.abservetech.jobstar;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Locale;

/**
 * Created by user on 9/21/2016.
 */
public class MainApplication extends Application {
  /*  @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String lang = preferences.getString("lang", "en");
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }*/

    public void onCreate() {
        super.onCreate();
        setResourceLocale(new Locale("ar"));
    }

    private void setResourceLocale(Locale locale) {
        if (Build.VERSION.SDK_INT >= 17) {
            getBaseContext().getResources().getConfiguration().setLocale(locale);
        } else {
            Configuration config = getResources().getConfiguration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }
    }}
