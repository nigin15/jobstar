package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/22/2016.
 */
public class Seeker_report  extends AppCompatActivity {
    TextView application_list,language,notification,about_this,rating,invite,report,signout;
    EditText message,title;
    String message1,title1;
    PrefManager prefManager;
    ProgressDialog pd;
    Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.report_problem);
        pd = new ProgressDialog(Seeker_report.this);
        prefManager = new PrefManager(this);
        message=(EditText) findViewById(R.id.message);
        title=(EditText) findViewById(R.id.title);
        save=(Button)findViewById(R.id.submit) ;

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message1=message.getText().toString();
                title1=title.getText().toString();
                new Report_async().execute();

            }
        });



    }
    class Report_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url2 + "rep";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


                 nameValuePairs.add(new BasicNameValuePair("user_id", "27"));
                nameValuePairs.add(new BasicNameValuePair("title", title1));
                nameValuePairs.add(new BasicNameValuePair("description",message1));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());

         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);

                final String message2 = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(Seeker_report.this,message2,Toast.LENGTH_SHORT).show();
                    }
                });
        /*        for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {



                        JSONObject jsonObject = jsonArray.getJSONObject(i1);



                        final String text=( jsonObject.getString("text").toString());



                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                }*/

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }}