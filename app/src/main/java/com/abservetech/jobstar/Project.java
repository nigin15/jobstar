package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Project extends AppCompatActivity {

    public static String edit;
    public static String Project_title;
    public static String Projectclient;
    public static String Started_working_from;
    public static String Worked_tills;
    public static String Details_project;
    public static String Project_location;
    public static String Project_site;
    public static String Nature_employment;
    public static String Team_size;
    public static String Role;
    public static String Role_description;
    public static String Skills_used;
    public static String id;
    TextView hide;
    LinearLayout hidedetail;
    EditText projecttitle, projectclient, startedfrom, projectworkedtill, projectdetail, projectlocation, teamsize, role, roledescription, skillused;
    String projecttitle1, projectclient1, startedfrom1, projectworkedtill1, projectdetail1,projectlocation1,project_site1,nature_employment1, teamsize1, roledescription1, skillused1;
    Button projectsave;
    ProgressDialog pd;
    String project_title,client,started_working_from,worked_tills,details_project,
            project_location,project_site,nature_employment,team_size,role1,role_description,skills_used;
    RadioGroup radioGroup,radioGroup1;
    RadioButton offsite,onsite,fulltime,partime,contract,radio,radio1;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        pd = new ProgressDialog(Project.this);
        prefManager = new PrefManager(this);
        hide = (TextView) findViewById(R.id.hide);
        hidedetail = (LinearLayout) findViewById(R.id.hidedetail);
        projecttitle = (EditText) findViewById(R.id.projecttitle);
        projectclient = (EditText) findViewById(R.id.projectclient);
        startedfrom = (EditText) findViewById(R.id.startedfrom);
        projectworkedtill = (EditText) findViewById(R.id.projectworktill);
        projectdetail = (EditText) findViewById(R.id.projectdetail);
        projectlocation = (EditText) findViewById(R.id.projectlocation);
        teamsize = (EditText) findViewById(R.id.projectsize);
        role = (EditText) findViewById(R.id.projectrole);
        roledescription = (EditText) findViewById(R.id.roledescripction);
        skillused = (EditText) findViewById(R.id.skillused);
        projectsave = (Button) findViewById(R.id.projectsave);
        radioGroup=(RadioGroup)findViewById(R.id.radiogroup);
        radioGroup1=(RadioGroup)findViewById(R.id.radiogroup1);

        offsite=(RadioButton)findViewById(R.id.offsite);
        onsite=(RadioButton)findViewById(R.id.onsite);

        fulltime=(RadioButton)findViewById(R.id.fulltime);
        partime=(RadioButton)findViewById(R.id.parttime);
        contract=(RadioButton)findViewById(R.id.contract);

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidedetail.setVisibility(View.VISIBLE);
                hide.setVisibility(View.GONE);
            }
        });
        projectsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                projecttitle1 = projecttitle.getText().toString();
                projectclient1 = projectclient.getText().toString();
                startedfrom1 = startedfrom.getText().toString();
                projectworkedtill1 = projectworkedtill.getText().toString();
                projectdetail1 = projectdetail.getText().toString();
                projectlocation1 = projectlocation.getText().toString();
                teamsize1 = teamsize.getText().toString();
                role1 = role.getText().toString();
                roledescription1 = roledescription.getText().toString();
                skillused1 = skillused.getText().toString();
                if(id!=null){
                    new Project_editasync().execute();
                }else{
                new Project_async().execute();}
            }
        });




        projecttitle.setText(Project_title);
        projectclient.setText(Projectclient);
        startedfrom.setText(Started_working_from);
        projectworkedtill.setText(Worked_tills);
        projectlocation.setText(Project_location);
        teamsize.setText(Team_size);
        roledescription.setText(Role_description);
        role.setText(Role);
        skillused.setText(Skills_used);

        if(Project_site!=null){
        if(Project_site.equals("Off Site")){
            offsite.setChecked(true);
        }
        else{
            onsite.setChecked(true);
        }}
        else{}
        if(Nature_employment!=null){

        if(Nature_employment.equals("Full Time")){
            fulltime.setChecked(true);
        }
         if(Nature_employment.equals("Part Time")){
            partime.setChecked(true);
        }
            if(Nature_employment.equals("Contract")){
            contract.setChecked(true);
        }}else{

        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup group, int checkedId)
                                                  {
                                                      radio = (RadioButton) findViewById(checkedId);
                                                   //   Toast.makeText(getBaseContext(), radio.getText(), Toast.LENGTH_SHORT).show();
                                                      nature_employment1=radio.getText().toString();
                                                  }
                                              }
        );

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup group, int checkedId)
                                                  {
                                                      radio1 = (RadioButton) findViewById(checkedId);
                                                  //    Toast.makeText(getBaseContext(), radio1.getText(), Toast.LENGTH_SHORT).show();
                                                      project_site1=radio1.getText().toString();
                                                  }
                                              }
        );



    }

    class Project_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("loading");
            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "projectinsert";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("project_title",projectclient1));
                nameValuePairs.add(new BasicNameValuePair("client",projectclient1));
                nameValuePairs.add(new BasicNameValuePair("started_working_from",startedfrom1));
                nameValuePairs.add(new BasicNameValuePair("worked_tills",projectworkedtill1));
                nameValuePairs.add(new BasicNameValuePair("details_project",projectdetail1));
                nameValuePairs.add(new BasicNameValuePair("project_location",projectlocation1));
                nameValuePairs.add(new BasicNameValuePair("project_site",project_site1 ));
                nameValuePairs.add(new BasicNameValuePair("nature_employment",nature_employment1 ));
                nameValuePairs.add(new BasicNameValuePair("team_size",teamsize1));
                nameValuePairs.add(new BasicNameValuePair("role",role1));
                nameValuePairs.add(new BasicNameValuePair("role_description",roledescription1));
                nameValuePairs.add(new BasicNameValuePair("skills_used",skillused1));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Project.this, Message, Toast.LENGTH_SHORT).show();
                        Intent i= new Intent(Project.this,Profile.class);
                        startActivity(i);

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

    pd.dismiss();
        }
    }
    class Project_editasync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "projectupdate";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("project_title",projectclient1));
                nameValuePairs.add(new BasicNameValuePair("client",projectclient1));
                nameValuePairs.add(new BasicNameValuePair("started_working_from",startedfrom1));
                nameValuePairs.add(new BasicNameValuePair("worked_tills",projectworkedtill1));
                nameValuePairs.add(new BasicNameValuePair("details_project",projectdetail1));
                nameValuePairs.add(new BasicNameValuePair("project_location",projectlocation1));
                nameValuePairs.add(new BasicNameValuePair("project_site",project_site1 ));
                nameValuePairs.add(new BasicNameValuePair("nature_employment",nature_employment1 ));
                nameValuePairs.add(new BasicNameValuePair("team_size",teamsize1));
                nameValuePairs.add(new BasicNameValuePair("role",role1));
                nameValuePairs.add(new BasicNameValuePair("role_description",roledescription1));
                nameValuePairs.add(new BasicNameValuePair("skills_used",skillused1));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Project.this, Message, Toast.LENGTH_SHORT).show();
                        Intent i= new Intent(Project.this,Profile.class);
                        startActivity(i);

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected



            case R.id.action_refresh:
                new Projectdelete().execute();
                break;
            // action with ID action_settings was selected

            default:
                break;
        }

        return true;
    }
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.action_refresh);


        if(edit!=null)
        {
            register.setVisible(true);
        }
        else
        {
            register.setVisible(false);
        }
        return true;
    }

    class Projectshow extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            pd.setMessage("loading");
            pd.show();
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "projectshow";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id", "27"));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "itskillsshowresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                JSONArray jsonArray = jsonRootObject.optJSONArray("Work_Experience_details");
                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
                        id = jsonObject.getString("id").toString();

                        project_title= jsonObject.getString("project_title").toString();
                        client= jsonObject.getString("client").toString();
                        started_working_from= jsonObject.getString("started_working_from").toString();
                        worked_tills= jsonObject.getString("worked_tills").toString();
                        details_project= jsonObject.getString("details_project").toString();
                        project_location= jsonObject.getString("project_location").toString();
                        project_site= jsonObject.getString("project_site").toString();
                        nature_employment= jsonObject.getString("nature_employment").toString();
                        team_size= jsonObject.getString("team_size").toString();
                        role1= jsonObject.getString("role").toString();
                        role_description= jsonObject.getString("role_description").toString();

                        skills_used= jsonObject.getString("skills_used").toString();

                    }

                    catch (Exception e){
                        e.printStackTrace();
                    }}


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
    class Projectdelete extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "projectdelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", id));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumedeleteresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String message = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Project.this,message,Toast.LENGTH_SHORT).show();

                        Intent i= new Intent(Project.this,Profile.class);
                        startActivity(i);
                    }});


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
}
