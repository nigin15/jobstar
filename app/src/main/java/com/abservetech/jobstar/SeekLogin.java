package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SeekLogin extends AppCompatActivity {
    EditText Email, password;
    RelativeLayout coordinatorLayout;
    ProgressDialog pd;
    PrefManager prefManager;
    Button loginsubmit,forgetpass;
    String email, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setContentView(R.layout.activity_seek_login);

        coordinatorLayout = (RelativeLayout) findViewById(R.id.coordinator);
        pd = new ProgressDialog(SeekLogin.this);
        Email = (EditText) findViewById(R.id.loginuser);
        password = (EditText) findViewById(R.id.loginpasswor);
        loginsubmit = (Button) findViewById(R.id.login);
        forgetpass = (Button) findViewById(R.id.forgetpass);
        prefManager = new PrefManager(this);
//        Email.setText("ss@gmail.com");
//        password.setText("qwerty");

        loginsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = Email.getText().toString();
                pass = password.getText().toString();
                validation(email, pass);
//                Intent log=new Intent(Login.this,Home.class);
//                startActivity(log);


            }
        });
        forgetpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(SeekLogin.this,Forget_password.class);
                startActivity(i);
            }
        });
    }
    private void validation(String email, String pass) {
//        if (Patterns.EMAIL_ADDRESS.matcher(this.email).matches()) {
            //Toast.makeText(this, " valid email", Toast.LENGTH_SHORT).show();

            if (this.email.length() >=6) {
            if (this.pass.length() > 0) {

                if (this.pass.length() >= 6 && this.pass.length() <= 12) {


//                    new Loginhh().execute(email, pass,regid);
                    new Loginhh().execute(email, pass);


                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Password must be 6-12", Snackbar.LENGTH_LONG);
                    View snackbarView = snackbar.getView();
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.RED);
                    snackbarView.setBackgroundColor(Color.WHITE);
                    snackbar.show();
                    password.setBackgroundResource(R.drawable.rounder_whiteedit);
//                    password.setError("Password must be 6-12");

                }
            } else {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Enter password", Snackbar.LENGTH_LONG);
                View snackbarView = snackbar.getView();
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.RED);
                snackbarView.setBackgroundColor(Color.WHITE);
                snackbar.show();
                password.setBackgroundResource(R.drawable.rounder_whiteedit);
                //  password.setError("Enter password");
            }
        } else {
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Enter valid Email or Phone no", Snackbar.LENGTH_LONG);
            View snackbarView = snackbar.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.RED);
            snackbarView.setBackgroundColor(Color.WHITE);
            snackbar.show();
            Email.setBackgroundResource(R.drawable.rounder_whiteedit);
            //  Email.setError("Enter valid Email");

        }
    }
    class Loginhh extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url=Serviceurl.url+"login";
            HttpPost httppost = new HttpPost(url);
            String group_id = "4";
            Intent i = getIntent();

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {

                nameValuePairs.add(new BasicNameValuePair("email", email));}
                else{
                    nameValuePairs.add(new BasicNameValuePair("phone_number", email));
                }
                nameValuePairs.add(new BasicNameValuePair("password", pass));
           //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                JSONArray jsonArray = jsonRootObject.optJSONArray("user_id");
                JSONArray jsonArray1 = jsonRootObject.optJSONArray("activation");
                String  first = jsonRootObject.getString("id");
                final String  message = jsonRootObject.getString("message");


//
                Log.d("first1", "response -----" + first);

                if (first.equals("1")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(SeekLogin.this, message, Toast.LENGTH_LONG).show();


                        }
                    });
                }

                if (first.equals("2")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(SeekLogin.this, message, Toast.LENGTH_LONG).show();

                        }
                    });
                }

                if (first.equals("3")) {
                    final String  user_id = jsonRootObject.getString("user_id");
                    JSONArray jsonArray3 = jsonRootObject.optJSONArray("user_details");

                    for (int i1 = 0; i1 < jsonArray3.length(); i1++) {
                        try {

                            JSONObject jsonObject = jsonArray3.getJSONObject(i1);
                           String username = jsonObject.getString("username").toString();
                            email= jsonObject.getString("email").toString();
                            HomePage.username1=username;
                            HomePage.email=email;
                        }
                        catch (Exception e){
                        e.printStackTrace();}
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            prefManager.setFirstTimeLogin(false);
                            prefManager.setuserid(user_id);
                            Toast.makeText(SeekLogin.this, "Login Sucessfully", Toast.LENGTH_LONG).show();
                            Intent bLogin = new Intent(SeekLogin.this, HomePage.class);
                            //bLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(bLogin);


                        }
                    });
                }
                if (first.equals("5")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SeekLogin.this, "Invalid Username", Toast.LENGTH_LONG).show();

                        }
                    });
                }
                if (first.equals("4")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SeekLogin.this, "username password combination was incorrect", Toast.LENGTH_LONG).show();

                        }
                    });
                }
          /*      for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
                        String id = jsonObject.getString("id").toString();
//                       prefManager.setuserid(id);
                        Log.d("id", "id -----" + id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                for (int i1 = 0; i1 < jsonArray1.length(); i1++) {
                    try {
                        JSONObject jsonObject = jsonArray1.getJSONObject(i1);
                        String act_id = jsonObject.getString("id").toString();
                     //   prefManager.setactid(act_id);
                        Log.d("id", "id -----" + act_id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }*/


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }



}
