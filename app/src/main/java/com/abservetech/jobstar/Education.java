package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class Education extends AppCompatActivity {
    public static String Education;
    public static String Course;
    public static String Specialization;
    public static String Institute;
    public static String Year_of_passing;
    public static String Time_periods;
    public static String id;
    public static String edit;
    EditText education,course,specialization,institute,passing;
    String education1,course1,specialization1,institute1,passing1;
    Button save;
    ProgressDialog pd;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_education);
        prefManager = new PrefManager(this);
        education=(EditText)findViewById(R.id.education);
        course=(EditText)findViewById(R.id.educourse);
        specialization=(EditText)findViewById(R.id.specialization);
        institute=(EditText)findViewById(R.id.institute);
        passing=(EditText)findViewById(R.id.passing);
        save=(Button)findViewById(R.id.save);
        pd = new ProgressDialog(Education.this);
        System.out.println("edit--"+edit);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                education1=education.getText().toString();
                course1=course.getText().toString();
                specialization1=specialization.getText().toString();
                institute1=institute.getText().toString();
                passing1=passing.getText().toString();
                if(id!=null){
                    new Educationedit_async().execute();
               }
                else{
                    new Education_async().execute();
                }
            }
        });

        education.setText(Education);
        course.setText(Course);
        specialization.setText(Specialization);
        institute.setText(Institute);
        passing.setText(Year_of_passing);

    }
    class Education_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "educationinsert";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("education", education1));
                nameValuePairs.add(new BasicNameValuePair("course", course1));
                nameValuePairs.add(new BasicNameValuePair("specialization", specialization1));
                nameValuePairs.add(new BasicNameValuePair("institute", institute1));
                nameValuePairs.add(new BasicNameValuePair("year_of_passing", passing1));
                nameValuePairs.add(new BasicNameValuePair("time_periods", "full time"));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");





                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Education.this,Message,Toast.LENGTH_SHORT).show();

                            Intent i=new Intent(Education.this,Profile.class);
                            startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class Educationedit_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "educationupdate";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("education", education1));
                nameValuePairs.add(new BasicNameValuePair("course", course1));
                nameValuePairs.add(new BasicNameValuePair("specialization", specialization1));
                nameValuePairs.add(new BasicNameValuePair("institute", institute1));
                nameValuePairs.add(new BasicNameValuePair("year_of_passing", passing1));
                nameValuePairs.add(new BasicNameValuePair("time_periods", "full time"));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");





                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Education.this,Message,Toast.LENGTH_SHORT).show();

                            Intent i=new Intent(Education.this,Profile.class);
                            startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class Educationdelete extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "itskillsdelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", id));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumedeleteresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String message = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Education.this,message,Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(Education.this,Profile.class);
                        startActivity(i);
                    }});


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected



            case R.id.action_refresh:
                new Educationdelete().execute();
                break;
            // action with ID action_settings was selected

            default:
                break;
        }

        return true;
    }
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.action_refresh);


        if(edit!=null)
        {
            register.setVisible(true);
        }
        else
        {
            register.setVisible(false);
        }
        return true;
    }
}
