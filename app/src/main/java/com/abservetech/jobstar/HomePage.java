package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.audiofx.BassBoost;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomePage extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static String username1;
    public static String email;
    GridView grid;
    ProgressDialog pd;
    TextView home_username, header_username, header_email;
    String[] web = {"Google","Github","Instagram","Facebook"} ;
    int[] imageId = {
            R.drawable.bg,
            R.drawable.bg,
            R.drawable.bg,
            R.drawable.bg,
    };

    TextView username,designation,recruiter_visit,recomended_job,recomended_desig,recomended_company,
    recriuter_job,saved_job,custom_alert,email_alert,applyrecomended_job,applyrecomended_desig,
            applyrecomended_company,apply_sent,seeall,recomended_search;
    CircleImageView user_image;
    LinearLayout recommended_layout,saved_layout,recruiter_layout,appliedjob_layout;
    PrefManager prefManager;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        pd = new ProgressDialog(HomePage.this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);

        header_username = (TextView) hView.findViewById(R.id.name);
        header_email = (TextView) hView.findViewById(R.id.desigination);
        username = (TextView) findViewById(R.id.username);
        designation = (TextView) findViewById(R.id.designation);
        recruiter_visit = (TextView) findViewById(R.id.recruiter_visit);
        recomended_job = (TextView) findViewById(R.id.recomended_job);
        recomended_desig = (TextView) findViewById(R.id.recomended_desig);
        recomended_company = (TextView) findViewById(R.id.recomended_company);
        recomended_search = (TextView) findViewById(R.id.recomended_search);
        recriuter_job = (TextView) findViewById(R.id.recriuter_job);
        saved_job = (TextView) findViewById(R.id.saved_job);
        custom_alert = (TextView) findViewById(R.id.custom_alert);
        email_alert = (TextView) findViewById(R.id.email_alert);
        applyrecomended_job = (TextView) findViewById(R.id.applyrecomended_job);
        applyrecomended_desig = (TextView) findViewById(R.id.applyrecomended_desig);
        applyrecomended_company = (TextView) findViewById(R.id.applyrecomended_company);
        apply_sent = (TextView) findViewById(R.id.apply_sent);
        seeall= (TextView) findViewById(R.id.seeall);
        user_image= (CircleImageView) findViewById(R.id.home_profile_image);
        recommended_layout=(LinearLayout)findViewById(R.id.recommended_layout);
        saved_layout=(LinearLayout)findViewById(R.id.saved_layout);
        recruiter_layout=(LinearLayout)findViewById(R.id.recruiter_layout);
        header_username.setText(username1);
        header_email.setText(email);
        new Homeshow().execute();
        new Homeshow1().execute();
        new Homeshow2().execute();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent i=new Intent(HomePage.this,HomeSearch.class);
                startActivity(i);
            }
        });

        recomended_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(HomePage.this,HomeSearch.class);
                startActivity(i);
            }
        });
        PrefManager prefManager = new PrefManager(this);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_to_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Homeshow().execute();
                new Homeshow1().execute();
                new Homeshow2().execute();
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);


        recommended_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(HomePage.this,Recommended_job.class);
                startActivity(i);
            }
        });
        saved_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(HomePage.this,Saved_jobs.class);
                startActivity(i);
            }
        });
        recruiter_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(HomePage.this,Recruiter_job.class);
                startActivity(i);
            }
        });

        seeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(HomePage.this,HomeApply_jobs.class);
                startActivity(i);
            }
        });



        }
    public void retrieveDocuments() {
//some code, server call

//onCompletion is your server response with a success

        }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Find) {
            // Handle the camera action
        } else if (id == R.id.Share) {

        } else if (id == R.id.Profile) {
            Intent i= new Intent(HomePage.this,Profile.class);
            startActivity(i);

        } else if (id == R.id.Settings) {
            Intent i= new Intent(HomePage.this,Seeker_settings.class);
            startActivity(i);

        } else if (id == R.id.Parttime) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    class Homeshow extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "home";
            HttpPost httppost = new HttpPost(url);
            String group_id = "4";
            Intent i = getIntent();

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));

                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.d("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "Homeshow1 -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);


                JSONArray jsonArray = jsonRootObject.optJSONArray("personal_details");

                JSONArray jsonArray1 = jsonRootObject.optJSONArray("Recommended_job_list");

                JSONArray jsonArray3 = jsonRootObject.optJSONArray("Saved_job_list");




                final String Saved_job_count = jsonRootObject.optString("Saved_job_count");
                final String Recommended_job_count = jsonRootObject.optString("Recommended_job_count");
                Recommended_job.count=Recommended_job_count;
                Saved_jobs.count=Saved_job_count;


                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
                        final  String username1 = jsonObject.getString("username").toString();
                        final  String avatar = jsonObject.getString("avatar").toString();
                        final  String designation1 = jsonObject.getString("designation").toString();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                username.setText(username1);

                                designation.setText(designation1);

                                try {
                                    URL url = new URL(avatar);

                                    Glide.with(HomePage.this).load(String.valueOf(url)).into(user_image);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if(Saved_job_count!=null){
                                    saved_job.setText(Saved_job_count);
                                }

                                if(Recommended_job_count!=null){
                                    recomended_job.setText(Recommended_job_count);
                                }



                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                Recommended_job.movieList = new ArrayList<HomepageGS>();
                for (int i1 = 0; i1 < jsonArray1.length(); i1++) {
                    try {



                        JSONObject jsonObject = jsonArray1.getJSONObject(i1);
                        HomepageGS movie=new HomepageGS();

                        movie.setjob_title( jsonObject.getString("job_title").toString());
                        movie.setcompany_name( jsonObject.getString("company_name").toString());
                        movie.setjob_experience( jsonObject.getString("job_experience").toString());
                        movie.setjob_location( jsonObject.getString("job_location").toString());
                        movie.setjob_specialization( jsonObject.getString("keyskills").toString());
                        movie.setjob_date( jsonObject.getString("date").toString());
                        movie.setjob_id( jsonObject.getString("id").toString());
                        Recommended_job.movieList.add(movie);



                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }



                Log.d("title--",""+jsonArray1);
                Saved_jobs.movieList = new ArrayList<HomepageGS>();
                for (int i1 = 0; i1 < jsonArray3.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray3.getJSONObject(i1);
                        HomepageGS movie=new HomepageGS();
                        System.out.print("title1234---"+"title++company");
                        movie.setsavedjob_title( jsonObject.getString("job_title").toString());
                        movie.setsavedcompany_name( jsonObject.getString("company_name").toString());
                        movie.setsavedjob_experience( jsonObject.getString("job_experience").toString());
                        movie.setsavedjob_location( jsonObject.getString("job_location").toString());
                        movie.setsavedjob_specialization( jsonObject.getString("keyskills").toString());
                        movie.setsavedjob_date( jsonObject.getString("date").toString());
                        movie.setsavedjob_id( jsonObject.getString("id").toString());
                        Saved_jobs.movieList.add(movie);


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


                if(jsonArray1!=null){

                    JSONObject jsonObject = jsonArray1.getJSONObject(0);
                   final String title=( jsonObject.getString("job_title").toString());
                   final String company=( jsonObject.getString("company_name").toString());
                    System.out.print("title123---"+title+" "+company);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recomended_desig.setVisibility(View.VISIBLE);
                            recomended_company.setVisibility(View.VISIBLE);
                            recomended_desig.setText(title);
                            recomended_company.setText(company);
                        }
                    });
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }

    class Homeshow1 extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "homenext";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",  prefManager.getuserid()));

                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "Homeshow2 -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);



                JSONArray jsonArray2 = jsonRootObject.optJSONArray("Job_offer_list");




                final String Job_offer_count = jsonRootObject.optString("Job_offer_count");

                Recruiter_job.count=Job_offer_count;

                Recruiter_job.movieList = new ArrayList<HomepageGS>();
                for (int i1 = 0; i1 < jsonArray2.length(); i1++) {
                    try {



                        JSONObject jsonObject = jsonArray2.getJSONObject(i1);
                        HomepageGS movie=new HomepageGS();

                        movie.setrecruiterjob_title( jsonObject.getString("job_title").toString());
                        movie.setrecruitercompany_name( jsonObject.getString("company_name").toString());
                        movie.setrecruiterjob_experience( jsonObject.getString("job_experience").toString());
                        movie.setrecruiterjob_location( jsonObject.getString("job_location").toString());
                        movie.setrecruiterjob_specialization( jsonObject.getString("keyskills").toString());
                        movie.setrecruiterjob_date( jsonObject.getString("date").toString());
                        movie.setrecruiterjob_id( jsonObject.getString("id").toString());
                        Recruiter_job.movieList.add(movie);


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(Job_offer_count!=null){
                                    recriuter_job.setText(Job_offer_count);
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }



            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }

    class Homeshow2 extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "homesecond";
            HttpPost httppost = new HttpPost(url);
            String group_id = "4";
            Intent i = getIntent();

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",  prefManager.getuserid()));

                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "Homeshow3 -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);



                JSONArray jsonArray4 = jsonRootObject.optJSONArray("Apply_job_list");




                final String Apply_job_count = jsonRootObject.optString("Apply_job_count");

                HomeApply_jobs.count=Apply_job_count;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(Apply_job_count!=null){
                            applyrecomended_job.setText(Apply_job_count);
                        }
                    }
                });

                if(jsonArray4.length()>=0){

                    JSONObject jsonObject = jsonArray4.getJSONObject(0);
                    final String title=( jsonObject.getString("job_title").toString());
                    final String company=( jsonObject.getString("company_name").toString());
                    System.out.print("title123---"+title+" "+company);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            applyrecomended_desig.setVisibility(View.VISIBLE);
                            applyrecomended_company.setVisibility(View.VISIBLE);
                            applyrecomended_desig.setText(title);
                            applyrecomended_company.setText(company);
                        }
                    });
                }


                HomeApply_jobs.movieList = new ArrayList<HomepageGS>();
                for (int i1 = 0; i1 < jsonArray4.length(); i1++) {
                    try {



                        JSONObject jsonObject = jsonArray4.getJSONObject(i1);
                        HomepageGS movie=new HomepageGS();

                        movie.setapplyjob_title( jsonObject.getString("job_title").toString());
                        movie.setapplycompany_name( jsonObject.getString("company_name").toString());
                        movie.setapplyjob_experience( jsonObject.getString("job_experience").toString());
                        movie.setapplyjob_location( jsonObject.getString("job_location").toString());
                        movie.setapplyjob_specialization( jsonObject.getString("keyskills").toString());
                        movie.setapplyjob_date( jsonObject.getString("date").toString());
                        movie.setapplyjob_id( jsonObject.getString("id").toString());
                        HomeApply_jobs.movieList.add(movie);



                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }

}
