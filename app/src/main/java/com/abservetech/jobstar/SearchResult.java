package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SearchResult extends AppCompatActivity {
    static String skill, location, salary, experience;
    String username1, designation1, location1, experience1, phno1, email1, salary1, userimage1,resumeheadline,profilesummary;
    public  static   List<HomepageGS> movieList = new ArrayList<HomepageGS>();
    ListView searchresultlist;
    HomeSearchAdapter adapter;
    TextView emptyview;
    ProgressDialog pd;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        pd = new ProgressDialog(SearchResult.this);
        searchresultlist=(ListView)findViewById(R.id.searchresult);
         emptyview=(TextView)findViewById(R.id.emptyview);
       /* if(count.equals("0")){
            emptyview.setVisibility(View.VISIBLE);
        }
        else{
            emptyview.setVisibility(View.GONE);
        }*/

        new Myprofileshow().execute(skill,location,salary,experience);
    }

    class Myprofileshow extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "seekersearch";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("search_text", skill));
                nameValuePairs.add(new BasicNameValuePair("job_location", location));
                nameValuePairs.add(new BasicNameValuePair("job_experience", experience));
                nameValuePairs.add(new BasicNameValuePair("base_salary", salary));

                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);


                JSONArray jsonArray = jsonRootObject.optJSONArray("Employers_details");
                try{

                final String message = jsonRootObject.optString("message");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                            if(message.equals("No Result found")){
                                emptyview.setText("No Result found");
                                emptyview.setVisibility(View.VISIBLE);
                            }
                    }});}
                catch (Exception e){
                    e.printStackTrace();
                }
                movieList.clear();

                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
                        HomepageGS movie=new HomepageGS();
                        movie.setsearchjob_title( jsonObject.getString("job_title").toString());
                        movie.setsearchcompany_name( jsonObject.getString("company_name").toString());
                        movie.setsearchjob_experience( jsonObject.getString("job_experience").toString());
                        movie.setsearchjob_location( jsonObject.getString("job_location").toString());
                        movie.setsearchjob_specialization( jsonObject.getString("keyskills").toString());
                        movie.setsearchjob_date( jsonObject.getString("date").toString());
                        movie.setsearchjob_id( jsonObject.getString("id").toString());
                        movie.setsearchjob_Saved( jsonObject.getString("Saved").toString());
                        movie.setsearchjob_Applied( jsonObject.getString("Applied").toString());



                        movieList.add(movie);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter=new HomeSearchAdapter(SearchResult.this, movieList);
                            searchresultlist.setAdapter(adapter);
                        }});
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);


            pd.dismiss();

        }
    }

    public class HomeSearchAdapter extends BaseAdapter {

        private List<HomepageGS> movieItems;
        SearchResult activity;


        public HomeSearchAdapter(SearchResult activity, List<HomepageGS> movieItems) {
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
//        userid = sharedPreferences.getString("userid",null);
//        currency1 = sharedPreferences.getString("currenycode",null);
//        System.out.println("userid in shared preferences in custom search adapter"+userid);

            this.activity = activity;
            this.movieItems = movieItems;
        }

        class ViewHolder {

            protected TextView date,designation,company,experience,location,skill;
            protected CheckBox wishlist;
            ImageView thumbNail;

        }
        @Override
        public int getCount() {
            return movieItems.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {

            ViewHolder viewHolder = null;
            if (convertView == null) {
                LayoutInflater inflator = LayoutInflater.from(parent.getContext());
                convertView = inflator.inflate(R.layout.job_list1, null);
                viewHolder = new ViewHolder();
                viewHolder. designation=(TextView)convertView.findViewById(R.id.textView5);
                viewHolder. company=(TextView)convertView.findViewById(R.id.textView7);
                viewHolder. experience=(TextView)convertView.findViewById(R.id.textView9);
                viewHolder. location=(TextView)convertView.findViewById(R.id.textView48);
                viewHolder. skill=(TextView)convertView.findViewById(R.id.textView56);
                viewHolder. date=(TextView)convertView.findViewById(R.id.date);

                convertView.setTag(viewHolder);

            }else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final HomepageGS m = movieItems.get(position);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent i= new Intent(activity,Job_detail.class);
                            Job_detail.id=m.getsearchjob_id();
                            Job_detail.saved=m.getsearchjob_Applied();
                            Job_detail.applied=m.getsearchjob_Saved();
                            startActivity(i);


                        }
                    });

                 /*   if(!count.equals("0")){
                      }
                    else{

                    }*/

                }
            });
            if (m.getsearchjob_title()!=null)
            {
                if (!m.getsearchjob_title().equals("null"))
                {
                    viewHolder. designation.setText(m.getsearchjob_title());
                }
            }

            if (m.getsearchjob_date()!=null)
            {
                if (!m.getsearchjob_date().equals("null"))
                {
                    viewHolder. date.setText(m.getsearchjob_date());
                }
            }
            if (m.getsearchcompany_name()!=null)
            {
                if (!m.getsearchcompany_name().equals("null"))
                {
                    viewHolder. company.setText(m.getsearchcompany_name());
                }
            }


            if (m.getsearchjob_experience()!=null)
            {
                if (!m.getsearchjob_experience().equals("null"))
                {
                    viewHolder. experience.setText(m.getsearchjob_experience());
                }
            }

            if (m.getsearchjob_location()!=null)
            {
                if (!m.getsearchjob_location().equals("null"))
                {
                    viewHolder. location.setText(m.getsearchjob_location());
                }
            }

            if (m.getsearchjob_specialization()!=null)
            {
                if (!m.getsearchjob_specialization().equals("null"))
                {
                    viewHolder. skill.setText(m.getsearchjob_specialization());
                }
            }

            return convertView;
        }
    }
}
