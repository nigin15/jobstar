package com.abservetech.jobstar;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Lincoln on 05/05/16.
 */
public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    Context context;
    public static final String MyPREFERENCES = "Userid";
    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "trackstra";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_FIRST_TIME_LOGIN = "IsFirstTimeLogin";
    public static final String ID = "userkey";
    public static final String RandomName = "randkey";
    public static final String Name = "namekey";
    public static final String Imagekey = "imagekey";
    public static final String Locationkey = "locationkey";
    public static final String SET_LOCATION_key = "setlocationkey";
    public static final String SET_FRIEND_key = "friendlocationkey";
    public static final String SET_PHONE_key = "sosphonekey";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLogin(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LOGIN, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLogin() {
        return pref.getBoolean(IS_FIRST_TIME_LOGIN, true);
    }

    public void setuserid(String id) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(ID, id);
        editor.commit();
    }

    public String getuserid() {

        return pref.getString(ID, "");
    }

    public void setlocarionid(boolean id) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(SET_LOCATION_key, id);
        editor.commit();
    }

    public boolean getlocationid() {
        return pref.getBoolean(SET_LOCATION_key, Boolean.parseBoolean(null));
    }

    public void setfriendid(boolean id) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(SET_FRIEND_key, id);
        editor.commit();
    }

    public boolean getfriendid() {
        return pref.getBoolean(SET_FRIEND_key,true);
    }

    public void setactid(String id) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(RandomName, id);
        editor.commit();
    }

    public String getactid() {

        return pref.getString(RandomName, "");
    }

    public void setname(String id) {
        Log.d("setname", id);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Name, id);
        editor.commit();
    }

    public String getname() {
        String s = pref.getString(Name, "");
        Log.d("getname", s);
        return s;
    }

    public void setimage(String id) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Imagekey, id);
        editor.commit();
        Log.d("setimage", id);
    }

    public String getimage() {
        String s = pref.getString(Imagekey, "");
        Log.d("getimage", s);
        return s;
    }

    public void setlocation(String id) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Locationkey, id);
        editor.commit();
    }

    public String getlocation() {

        return pref.getString(Locationkey, "");
    }
    public void setphone(String id) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SET_PHONE_key, id);
        editor.commit();
        Log.d("setphone", id);
    }

    public String getphone() {

        String s = pref.getString(SET_PHONE_key, "");
        Log.d("getphone", s);
        return s;
    }

    public void logout() {
        pref.edit().remove(ID).commit();
        pref.edit().remove(RandomName).commit();
        pref.edit().remove(IS_FIRST_TIME_LOGIN).commit();
        pref.edit().remove(Name).commit();
        pref.edit().remove(Imagekey).commit();
        pref.edit().remove(Locationkey).commit();
    }


}
