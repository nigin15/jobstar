package com.abservetech.jobstar;

/**
 * Created by ABSERVETECH on 02-Sep-16.
 */
public class MainPageGS {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getJob_type() {
        return job_type;
    }

    public void setJob_type(String job_type) {
        this.job_type = job_type;
    }

    public String getJob_level() {
        return job_level;
    }

    public void setJob_level(String job_level) {
        this.job_level = job_level;
    }

    public String getJob_country() {
        return job_country;
    }

    public void setJob_country(String job_country) {
        this.job_country = job_country;
    }

    public String getMin_qualification() {
        return min_qualification;
    }

    public void setMin_qualification(String min_qualification) {
        this.min_qualification = min_qualification;
    }

    public String getSalary_from() {
        return salary_from;
    }

    public void setSalary_from(String salary_from) {
        this.salary_from = salary_from;
    }

    public String getSalary_to() {
        return salary_to;
    }

    public void setSalary_to(String salary_to) {
        this.salary_to = salary_to;
    }

    public String getAvailable_slots() {
        return available_slots;
    }

    public void setAvailable_slots(String available_slots) {
        this.available_slots = available_slots;
    }

    public String getHide_company() {
        return hide_company;
    }

    public void setHide_company(String hide_company) {
        this.hide_company = hide_company;
    }

    public String getShow_salary() {
        return show_salary;
    }

    public void setShow_salary(String show_salary) {
        this.show_salary = show_salary;
    }

    public String getJob_desc() {
        return job_desc;
    }

    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }

    public String getNo_of_posts() {
        return no_of_posts;
    }

    public void setNo_of_posts(String no_of_posts) {
        this.no_of_posts = no_of_posts;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClosed_status() {
        return closed_status;
    }

    public void setClosed_status(String closed_status) {
        this.closed_status = closed_status;
    }

    public String getJob_state() {
        return job_state;
    }

    public void setJob_state(String job_state) {
        this.job_state = job_state;
    }

    public String getJob_location() {
        return job_location;
    }

    public void setJob_location(String job_location) {
        this.job_location = job_location;
    }

    public String getJob_experience() {
        return job_experience;
    }

    public void setJob_experience(String job_experience) {
        this.job_experience = job_experience;
    }

    public String getJob_specialization() {
        return job_specialization;
    }

    public void setJob_specialization(String job_specialization) {
        this.job_specialization = job_specialization;
    }

    String id,user_id,product_id,job_title,job_type,job_level,job_country,min_qualification,salary_from,salary_to,available_slots,hide_company;
    String show_salary,job_desc,no_of_posts,created,status,closed_status,job_state,job_location,job_experience,job_specialization;
}
