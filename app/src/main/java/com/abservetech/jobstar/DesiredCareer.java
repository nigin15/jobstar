package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DesiredCareer extends AppCompatActivity {
    public static String Industry_type;
    public static String Department;
    public static String Role;
    public static String Desired_job_type;
    public static String Desired_employment_type;
    public static String Preferred_location;
    public static String Work_permit_usa;
    public static String Work_permit;
    public static String id;
    public static String edit;
    EditText industry,department,role,location,usa_permit,other_permit;

    String industry1,department1,role1,location1,usa_permit1,other_permit1,permanent1,jobtype;
    CheckBox permanent,contract,fulltime,parttime;
    Button save;
    ProgressDialog pd;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desired_career);

        prefManager = new PrefManager(this);

        pd = new ProgressDialog(DesiredCareer.this);
        usa_permit=(EditText)findViewById(R.id.usa_permit);
        other_permit=(EditText)findViewById(R.id.other_permit);
        industry=(EditText)findViewById(R.id.industry);
        department=(EditText)findViewById(R.id.department);
        role=(EditText)findViewById(R.id.role);
        location=(EditText)findViewById(R.id.location);

        permanent=(CheckBox)findViewById(R.id.permanent);
        contract=(CheckBox)findViewById(R.id.contract);
        fulltime=(CheckBox)findViewById(R.id.fulltime);
        parttime=(CheckBox)findViewById(R.id.parttime);
        save=(Button)findViewById(R.id.button2);

        industry.setText(Industry_type);
        department.setText(Department);
        role.setText(Role);
        location.setText(Preferred_location);
        usa_permit.setText(Work_permit_usa);
        other_permit.setText(Work_permit);

        if(Desired_job_type!=null){
            if(Desired_job_type.equals("permanent")){
                permanent.setChecked(true);
            }
            if(Desired_job_type.equals("contract")){
                contract.setChecked(true);
            }
        }

        if(Desired_employment_type!=null){
            if(Desired_employment_type.equals("fulltime")){
                fulltime.setChecked(true);
            }
            if(Desired_employment_type.equals("parttime")){
                parttime.setChecked(true);
            }
        }


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(permanent.isChecked()){
                    permanent1="permanent";
                }
                if(contract.isChecked()){
                    permanent1="contract";
                }
                if(fulltime.isChecked()){
                    jobtype="fulltime";
                }
                if(parttime.isChecked()){
                    jobtype="parttime";
                }

/*                industry1,department1,role1,location1,usa_permit1,other_permit1,*/

                industry1=industry.getText().toString();
                department1=department.getText().toString();
                role1=role.getText().toString();
                location1=location.getText().toString();
                usa_permit1=usa_permit.getText().toString();
                other_permit1=other_permit.getText().toString();
                if(id!=null){
                    new desieredcareer_edit().execute();
                }
                else{   new desieredcareer_async().execute();}

            }
        });





    }

    class desieredcareer_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "workdetailinsert";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));

               /* user_id,industry_type,department,roles,desired_job_type,desired_employment_type,
                        preferred_location,available_to_join,work_permit_usa,work_permit*/
                nameValuePairs.add(new BasicNameValuePair("industry_type", industry1));

                nameValuePairs.add(new BasicNameValuePair("department", department1));
                nameValuePairs.add(new BasicNameValuePair("roles", role1));
                nameValuePairs.add(new BasicNameValuePair("desired_job_type",permanent1 ));
                nameValuePairs.add(new BasicNameValuePair("desired_employment_type",jobtype ));
                nameValuePairs.add(new BasicNameValuePair("preferred_location", location1));
                nameValuePairs.add(new BasicNameValuePair("available_to_join", industry1));
                nameValuePairs.add(new BasicNameValuePair("work_permit_usa", usa_permit1));
                nameValuePairs.add(new BasicNameValuePair("work_permit", other_permit1));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");


//                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString(Name, id);

//
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(DesiredCareer.this,Message,Toast.LENGTH_SHORT).show();
                        if(Message.equals("your other_certifications update successfully")){
                            Intent i=new Intent(DesiredCareer.this,Profile.class);
                            startActivity(i);
                        }
                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }


    class desieredcareer_edit extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "workdetailupdate";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));

               /* user_id,industry_type,department,roles,desired_job_type,desired_employment_type,
                        preferred_location,available_to_join,work_permit_usa,work_permit*/
                nameValuePairs.add(new BasicNameValuePair("industry_type", industry1));

                nameValuePairs.add(new BasicNameValuePair("department", department1));
                nameValuePairs.add(new BasicNameValuePair("roles", role1));
                nameValuePairs.add(new BasicNameValuePair("desired_job_type",permanent1 ));
                nameValuePairs.add(new BasicNameValuePair("desired_employment_type",jobtype ));
                nameValuePairs.add(new BasicNameValuePair("preferred_location", location1));
                nameValuePairs.add(new BasicNameValuePair("available_to_join", industry1));
                nameValuePairs.add(new BasicNameValuePair("work_permit_usa", usa_permit1));
                nameValuePairs.add(new BasicNameValuePair("work_permit", other_permit1));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");


//                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString(Name, id);

//
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(DesiredCareer.this,Message,Toast.LENGTH_SHORT).show();

                            Intent i=new Intent(DesiredCareer.this,Profile.class);
                            startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class desieredcareer_delete extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "workdetaildelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", id));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumedeleteresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String message = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(DesiredCareer.this,message,Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(DesiredCareer.this,Profile.class);
                        startActivity(i);
                    }});


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected



            case R.id.action_refresh:
                new desieredcareer_delete().execute();
                break;
            // action with ID action_settings was selected

            default:
                break;
        }

        return true;
    }
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.action_refresh);


        if(edit!=null)
        {
            register.setVisible(true);
        }
        else
        {
            register.setVisible(false);
        }
        return true;
    }}