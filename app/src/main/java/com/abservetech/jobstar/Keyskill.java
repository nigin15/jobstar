package com.abservetech.jobstar;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/7/2016.
 */
public class Keyskill extends AppCompatActivity {
    Button buttonskill;
    EditText keyskill_edit;
    String headline;
    TextView countleng;
    RelativeLayout savelayout;
    ListView searchlist;
    GridView viewlist;
    List<String> mainpagelist = new ArrayList<String>();
    ArrayAdapter<String> dataAdapter = null;
    List<String> viewpagelist = new ArrayList<String>();
    ArrayAdapter<String> dataviewAdapter = null;
    PrefManager prefManager;
    ProgressDialog pd;
    private List<ProfileGS> keyskill = new ArrayList<ProfileGS>();
    Keyskilladapter adapter;
    ArrayList<String> list = new ArrayList<String>();
    public static ActionMode mActionMode;
    String key1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyskill);
        pd = new ProgressDialog(Keyskill.this);
        prefManager = new PrefManager(this);
        keyskill_edit = (EditText) findViewById(R.id.keyskill_edit);
        buttonskill = (Button) findViewById(R.id.buttonskill);
        countleng = (TextView) findViewById(R.id.count);
        searchlist = (ListView) findViewById(R.id.listView9);
        savelayout = (RelativeLayout) findViewById(R.id.savelayout);
        viewlist = (GridView) findViewById(R.id.viewkeyskil);
        new keyskillview().execute();
        buttonskill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                headline = keyskill_edit.getText().toString();
                if(headline.length()>0){
                new keyskillsave().execute(headline);
                }
                else{
                    Toast.makeText(Keyskill.this,"Please Add Your skill",Toast.LENGTH_SHORT).show();
                }
            }
        });
        keyskill_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savelayout.setVisibility(View.VISIBLE);
                searchlist.setVisibility(View.GONE);
            }
        });


        keyskill_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                headline = s.toString();


                new keyskillsearch().execute(headline);
                savelayout.setVisibility(View.VISIBLE);
                searchlist.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                countleng.setText(String.valueOf(s.length()));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        searchlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String val = (String) (searchlist.getItemAtPosition(position));
                keyskill_edit.setText(val);
                savelayout.setVisibility(View.VISIBLE);
                searchlist.setVisibility(View.GONE);
            }
        });


   /*     viewlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                viewlist.setBackgroundColor(getResources().getColor(R.color.white));
            }
        });*/

        viewlist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        viewlist.setMultiChoiceModeListener(new  AbsListView.MultiChoiceModeListener() {

            @Override

            public boolean  onPrepareActionMode(ActionMode mode, Menu menu) {

                // TODO  Auto-generated method stub

                return false;

            }

            @Override

            public void  onDestroyActionMode(ActionMode mode) {

                // TODO  Auto-generated method stub

                // action.setDisplayHomeAsUpEnabled(true);
                adapter.removeSelection();
                // mode.finish();
                //dissmiss the action bar in android fragment
            }



            @Override

            public boolean  onCreateActionMode(ActionMode mode, Menu menu) {

                // TODO  Auto-generated method stub
                mActionMode = mode;
                mActionMode.getMenuInflater().inflate(R.menu.multiple_delete, menu);

                return true;

            }

            @Override

            public boolean  onActionItemClicked(final ActionMode mode,

                                                final MenuItem item) {

                // TODO  Auto-generated method stub

                switch  (item.getItemId()) {

                    case R.id.selectAll:

                        final int checkedCount  = keyskill.size();

                        // If item  is already selected or checked then remove or
                        // unchecked  and again select all

                        for (int i = 0; i < checkedCount; i++) {

                            viewlist.setItemChecked(i, true);

                            //  listviewadapter.toggleSelection(i);
                        }

                        mode.setTitle(checkedCount  + "  Selected");


                        return true;
                    case R.id.unselect:
                        mode.finish();
                        return false;


                    case R.id.delete:

                        // Add  dialog for confirmation to delete selected item
                        // record.
                        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Keyskill.this, R.style.AppCompatAlertDialogStyle);
                        builder.setMessage("Do you  want to delete selected record(s)?");
                        builder.setNegativeButton("No", new  DialogInterface.OnClickListener() {



                            @Override

                            public void  onClick(DialogInterface dialog, int which) {

                                // TODO  Auto-generated method stub
                            }

                        });

                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {


                            @Override

                            public void onClick(DialogInterface dialog, int which) {

                                // TODO  Auto-generated method stub

                                SparseBooleanArray selected = adapter

                                        .getSelectedIds();


                                for (int i = (selected.size()); i >= 0; i--) {

                                    if (selected.valueAt(i)) {
//                                        String selecteditem=Integer.parseInt(selected.keyAt(i));
//
                                        ProfileGS movie = keyskill.get(selected.keyAt(i));


                                        list.add(movie.getKeyskillid());

                                        System.out.println("EVENT ID____"+list);


                                        int  selecteditem = selected.keyAt(i);
                                        // String selecteditem1=Integer.toString(item.getItemId());
                                        int selecteditem1=item.getItemId();


                                        System.out.println("idd"+selecteditem1);

                                        dialog.dismiss();
                                        // Remove  selected items following the ids

                                        adapter.remove(movie);
                                        System.out.println("FEEDLIST SIZE+++"+keyskill.size());
                                     /*   if(feedsList.size()==0){
                                            emptyview.setVisibility(View.VISIBLE);
                                        }
                                        else{
                                            emptyview.setVisibility(View.GONE);
                                        }*/
                                    }
                                }
                                StringBuilder builder = new StringBuilder();
                                for (String value : list) {
                                    builder.append(value+",");
                                }
                                String text = builder.toString();
                                System.out.println("text ID____"+text);
                                 key1= text.substring(0, text.length() - 1);
                                new Keyskilldelete().execute();
                                list.clear();
                                // Close CAB
                                mode.finish();
                                selected.clear();
                            }

                        });

                        AlertDialog alert =  builder.create();
                        //alert.setIcon(R.drawable.questionicon);// dialog  Icon
                        alert.setTitle("Confirmation"); // dialog  Title
                        alert.show();
                        return true;
                    default:
                        mode.finish();
                        return false;

                }


            }

            @Override

            public void  onItemCheckedStateChanged(ActionMode mode,

                                                   int position, long id, boolean checked) {

                // TODO  Auto-generated method stub

                final int checkedCount  = viewlist.getCheckedItemCount();
                // Set the  CAB title according to total checked items
                mode.setTitle(checkedCount  + "  Selected");
                // Calls  toggleSelection method from ListViewAdapter Class
                adapter.toggleSelection(position);

            }
        });

    }

    class keyskillsearch extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "keysearch";
            HttpPost httppost = new HttpPost(url);


            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   "27"));
                nameValuePairs.add(new BasicNameValuePair("key_skills", headline));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);
                JSONObject jsonRootObject = new JSONObject(responseServer);
                JSONArray jsonArray = jsonRootObject.optJSONArray("key_skills_details");

                mainpagelist.clear();
                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {
//

                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
//

                        String key_skills = jsonObject.optString("key_skills").toString();
                        mainpagelist.add(key_skills);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dataAdapter = new ArrayAdapter<String>(Keyskill.this, R.layout.search_list, mainpagelist);
                                searchlist.setAdapter(dataAdapter);

                                //enables filtering for the contents of the given ListView
                                searchlist.setTextFilterEnabled(true);
                            }
                        });


//
                    } catch (JSONException e) {
//
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

        }
    }

    class keyskillsave extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("loading");
            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "keyskillinsert";
            HttpPost httppost = new HttpPost(url);


            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   "27"));
                nameValuePairs.add(new BasicNameValuePair("key_skills", headline));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);
                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String jsonArray = jsonRootObject.optString("Message");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Keyskill.this,jsonArray,Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Keyskill.this,Keyskill.class);
                        startActivity(i);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
                pd.dismiss();
        }
    }

    class keyskillview extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "keyskillshow";
            HttpPost httppost = new HttpPost(url);


            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",  "27"));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);
                JSONObject jsonRootObject = new JSONObject(responseServer);
                JSONArray jsonArray = jsonRootObject.optJSONArray("key_skills_details");


                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {
//


//
                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
                        ProfileGS profile1=new ProfileGS();
//

                        profile1.setKeyskillname(jsonObject.optString("key_skills").toString());
                        profile1.setKeyskillid(jsonObject.optString("id").toString());
                        keyskill.add(profile1);


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                    /*            dataviewAdapter = new ArrayAdapter<String>(Keyskill.this, R.layout.view_list, viewpagelist);
                                viewlist.setAdapter(dataviewAdapter);*/

                                adapter=new Keyskilladapter(Keyskill.this,keyskill);
                                viewlist.setAdapter(adapter);
                                //enables filtering for the contents of the given ListView
                                viewlist.setTextFilterEnabled(true);
                            }
                        });
                    } catch (Exception e) {
                        e.getMessage();


//
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

        }
    }

    public class Keyskilladapter extends BaseAdapter {

        private SparseBooleanArray mSelectedItemsIds;
        public Context context;
        private List<ProfileGS> movieItems;

        Bitmap bitmap;
        public Keyskilladapter(Context context, List<ProfileGS  > movieItems) {
            this.context = context;
            this.movieItems = movieItems;
            this.mSelectedItemsIds = new  SparseBooleanArray();

        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View gridView;

            if (convertView == null) {

                gridView = new View(context);

                // get layout from mobile.xml
                gridView = inflater.inflate(R.layout.keylist, null);
                TextView button1=(TextView)gridView.findViewById(R.id.button1);
                final CheckBox checkbox=(CheckBox)gridView.findViewById(R.id.checkbox);
                final ProfileGS m = movieItems.get(position);
                checkbox.setVisibility(View.GONE);

                if (m.getKeyskillname()!=null)
                {
                    if (!m.getKeyskillname().equals("null"))
                    {
                        button1. setText(m.getKeyskillname());
                    }
                }


                final View finalGridView = gridView;
              /*  gridView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        checkbox.setVisibility(View.VISIBLE);
                        finalGridView.setBackgroundColor(getResources().getColor(R.color.white));
                        return false;
                    }
                });*/

            } else {
                gridView = (View) convertView;
            }

            return gridView;
        }

        @Override
        public int getCount() {
            return movieItems.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void  toggleSelection(int position) {
            selectView(position, !mSelectedItemsIds.get(position));
        }

        public void  removeSelection() {
            mSelectedItemsIds = new SparseBooleanArray();
            notifyDataSetChanged();

        }


        public void selectView(int position, boolean value) {

            if (value)

                mSelectedItemsIds.put(position,  value);

            else

                mSelectedItemsIds.delete(position);

            notifyDataSetChanged();

        }

        public void remove(ProfileGS object) {
            movieItems.remove(object);
            notifyDataSetChanged();
        }
    /*    public void remove(int  object) {

            movieItems.remove(object);

            notifyDataSetChanged();

        }*/
        public  SparseBooleanArray getSelectedIds() {

            return mSelectedItemsIds;

        }
    }

    class Keyskilldelete extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "keyskilldelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", key1));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumedeleteresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String message = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Keyskill.this,message,Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(Keyskill.this,Keyskill.class);
                        startActivity(i);
                    }});


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
}
