package com.abservetech.jobstar;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class HomeSearch extends AppCompatActivity {

    EditText searchkey;
    Button searchjobs;
    ListView searchlist;
    Spinner spinner, spinner1, spinner2;
    List<String> mainpagelist = new ArrayList<String>();
    ArrayAdapter<String> dataAdapter = null;
    LinearLayout hidelayout;

    String headline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_search);

        searchkey = (EditText) findViewById(R.id.searchkey);
        hidelayout = (LinearLayout) findViewById(R.id.hidelayout);
        searchjobs = (Button) findViewById(R.id.searchjobs);
        searchlist = (ListView) findViewById(R.id.homesearchlist);
        spinner = (Spinner) findViewById(R.id.hsexperience);
        spinner1 = (Spinner) findViewById(R.id.hssalary);
        spinner2 = (Spinner) findViewById(R.id.hslocation);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.experience_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.salary_array, android.R.layout.simple_spinner_item);

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.india_top_places, android.R.layout.simple_spinner_item);

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner1.setAdapter(adapter1);
        spinner2.setAdapter(adapter2);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        searchjobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchResult.skill = searchkey.getText().toString();
                SearchResult.location = spinner2.getSelectedItem().toString();
                SearchResult.salary = spinner1.getSelectedItem().toString();
                SearchResult.experience = spinner.getSelectedItem().toString();
                Intent i = new Intent(HomeSearch.this, SearchResult.class);
                startActivity(i);
            }
        });

        searchkey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidelayout.setVisibility(View.GONE);
                searchlist.setVisibility(View.VISIBLE);
            }
        });
        searchkey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                headline = s.toString();


                new searchkillskill().execute(headline);
                hidelayout.setVisibility(View.GONE);
                searchlist.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        searchlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String val = (String) (searchlist.getItemAtPosition(position));
                searchkey.setText(val);
                hidelayout.setVisibility(View.VISIBLE);
                searchlist.setVisibility(View.GONE);
            }
        });
    }

    class searchkillskill extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "keysearch";
            HttpPost httppost = new HttpPost(url);


            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id", "27"));
                nameValuePairs.add(new BasicNameValuePair("key_skills", headline));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);
                JSONObject jsonRootObject = new JSONObject(responseServer);
                JSONArray jsonArray = jsonRootObject.optJSONArray("key_skills_details");

                mainpagelist.clear();
                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {
//

                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
//

                        String key_skills = jsonObject.optString("key_skills").toString();
                        mainpagelist.add(key_skills);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dataAdapter = new ArrayAdapter<String>(HomeSearch.this, R.layout.search_list, mainpagelist);
                                searchlist.setAdapter(dataAdapter);

                                //enables filtering for the contents of the given ListView
                                searchlist.setTextFilterEnabled(true);
                            }
                        });


//
                    } catch (JSONException e) {
//
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

        }
    }



}
