package com.abservetech.jobstar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/14/2016.
 */
public class Saved_jobs extends Activity{

    public static String count;
    ImageView back;
    SavedAdapter adapter;
    ListView recommended_listView;
    public  static   List<HomepageGS> movieList = new ArrayList<HomepageGS>();
    TextView Chckin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reccomended_job);

        back=(ImageView)findViewById(R.id.back);
        recommended_listView=(ListView)findViewById(R.id.recommended_listView);
        Chckin=(TextView)findViewById(R.id.Chckin);
        Chckin.setText("Saved Jobs");
        TextView emptyview=(TextView)findViewById(R.id.emptyview);
        if(count.equals("0")){
            emptyview.setVisibility(View.VISIBLE);
        }
        else{
            emptyview.setVisibility(View.GONE);
        }
        adapter=new SavedAdapter(Saved_jobs.this, movieList);
        recommended_listView.setAdapter(adapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }




    public class SavedAdapter extends BaseAdapter {

        private List<HomepageGS> movieItems;
        Saved_jobs activity;


        public SavedAdapter(Saved_jobs activity, List<HomepageGS> movieItems) {
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
//        userid = sharedPreferences.getString("userid",null);
//        currency1 = sharedPreferences.getString("currenycode",null);
//        System.out.println("userid in shared preferences in custom search adapter"+userid);

            this.activity = activity;
            this.movieItems = movieItems;
        }

        class ViewHolder {

            protected TextView designation,company,experience,location,skill,date;
            protected CheckBox wishlist;
            ImageView thumbNail;

        }
        @Override
        public int getCount() {
            return movieItems.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                LayoutInflater inflator = LayoutInflater.from(parent.getContext());
                convertView = inflator.inflate(R.layout.job_list1, null);
                viewHolder = new ViewHolder();
                viewHolder. designation=(TextView)convertView.findViewById(R.id.textView5);
                viewHolder. company=(TextView)convertView.findViewById(R.id.textView7);
                viewHolder. experience=(TextView)convertView.findViewById(R.id.textView9);
                viewHolder. location=(TextView)convertView.findViewById(R.id.textView48);
                viewHolder. skill=(TextView)convertView.findViewById(R.id.textView56);
                viewHolder.date=(TextView)convertView.findViewById(R.id.date);

                convertView.setTag(viewHolder);

            }else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final HomepageGS m = movieItems.get(position);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!count.equals("0")){
                    Intent i= new Intent(activity,Job_detail.class);
                    Job_detail.id=m.getsavedjob_id();
                     Job_detail.fromsaved="saved";
                    startActivity(i);}
                    else{

                    }

                }
            });
            if (m.getsavedjob_title()!=null)
            {
                if (!m.getsavedjob_title().equals("null"))
                {
                    viewHolder. designation.setText(m.getsavedjob_title());
                }
            }
            if (m.getsavedjob_date()!=null)
            {
                if (!m.getsavedjob_date().equals("null"))
                {

                    viewHolder. date.setText("Posted on "+m.getsavedjob_date());
                }
            }

            if (m.getsavedcompany_name()!=null)
            {
                if (!m.getsavedcompany_name().equals("null"))
                {
                    viewHolder. company.setText(m.getsavedcompany_name());
                }
            }


            if (m.getsavedjob_experience()!=null)
            {
                if (!m.getsavedjob_experience().equals("null"))
                {
                    viewHolder. experience.setText(m.getsavedjob_experience()+" Years");
                }
            }

            if (m.getsavedjob_location()!=null)
            {
                if (!m.getsavedjob_location().equals("null"))
                {
                    viewHolder. location.setText(m.getsavedjob_location());
                }
            }

            if (m.getsavedjob_specialization()!=null)
            {
                if (!m.getsavedjob_specialization().equals("null"))
                {
                    viewHolder. skill.setText(m.getsavedjob_specialization());
                }
            }

            return convertView;
        }
    }
}
