package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Language extends AppCompatActivity {

    public static String Languages;
    public static String Lan_proficiency;
    public static String Read;
    public static String Write;
    public static String Speak;
    public static String id;
    public static String edit;
    PrefManager prefManager;
    CheckBox radioButton,radioButton2,radioButton3;
    EditText language1,lang_proficency;
    Button save;
    String language2,proficency,read,write,speak;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        pd = new ProgressDialog(Language.this);
        prefManager = new PrefManager(this);
        radioButton=(CheckBox)findViewById(R.id.radioButton);
        radioButton2=(CheckBox)findViewById(R.id.radioButton2);
        radioButton3=(CheckBox)findViewById(R.id.radioButton3);
        language1=(EditText)findViewById(R.id.language1);
        lang_proficency=(EditText)findViewById(R.id.lang_proficency);
        save=(Button)findViewById(R.id.button2);

        language1.setText(Languages);
        lang_proficency.setText(Lan_proficiency);
        if(Read!=null){
            radioButton.setChecked(true);
        }
        if(Write!=null){
            radioButton2.setChecked(true);
        }
        if(Speak!=null){
            radioButton3.setChecked(true);
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                language2=language1.getText().toString();
                proficency=lang_proficency.getText().toString();
                if(radioButton.isChecked()){
                    read="read";

                }
                if(radioButton2.isChecked()){
                    write="write";

                }
                if(radioButton3.isChecked()){
                    speak="speak";

                }
                if(id!=null){
               new Languageedit_async().execute();
                }
                else{
                new Language_async().execute();
            }

            }
        });

    }
    class Language_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "languageinsert";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */



                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("languages", language2));
                nameValuePairs.add(new BasicNameValuePair("lan_proficiency", proficency));
                nameValuePairs.add(new BasicNameValuePair("read", read));
                nameValuePairs.add(new BasicNameValuePair("write", write));
                nameValuePairs.add(new BasicNameValuePair("speak", speak));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");





                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Language.this,Message,Toast.LENGTH_SHORT).show();

                        Intent i=new Intent(Language.this,Profile.class);
                        startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class Languageedit_async extends AsyncTask<String, Void, String>    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "languageupdate";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("languages", language2));
                nameValuePairs.add(new BasicNameValuePair("lan_proficiency", proficency));
                nameValuePairs.add(new BasicNameValuePair("read", read));
                nameValuePairs.add(new BasicNameValuePair("write", write));
                nameValuePairs.add(new BasicNameValuePair("speak", speak));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");





                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Language.this,Message,Toast.LENGTH_SHORT).show();

                        Intent i=new Intent(Language.this,Profile.class);
                        startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class Languagedelete extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "languagedelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", id));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumedeleteresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String message = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Language.this,message,Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(Language.this,Profile.class);
                        startActivity(i);
                    }});


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected



            case R.id.action_refresh:
                new Languagedelete().execute();
                break;
            // action with ID action_settings was selected

            default:
                break;
        }

        return true;
    }
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.action_refresh);


        if(edit!=null)
        {
            register.setVisible(true);
        }
        else
        {
            register.setVisible(false);
        }
        return true;
    }}
