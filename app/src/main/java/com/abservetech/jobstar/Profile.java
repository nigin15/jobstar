package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Profile extends AppCompatActivity {
    ProgressDialog pd;
    TextView username,edireshead, designation, location, experience, phno, email, salary, resume_headline, profile_summary, keyskill, employmentlist,projectedit;
    String username1, designation1, location1, experience1, phno1, email1, salary1, userimage1, resumeheadline, profilesummary;
    String industry_type, department, roles, desired_job_type, desired_employment_type, preferred_location, available_to_join, work_permit_usa, work_permit, date_of_birth, gender, home_town, pincode, marital_status, permanent_address;

    CircleImageView home_profile_image;
    RelativeLayout profile_layout;
    ImageView edit_button;
    StringBuilder string1 = new StringBuilder();
    RecyclerView employeelist, itlist, projctlist, edulist, desirredlist, otherlist, langlist;
    ProfileAdapter profileAdapter;
    ITAdapter itAdapter;
    EDUAdapter eduAdapter;
    ProjectAdapter projectAdapter;
    DesireAdapter desireAdapter;
    OtherAdapter otherAdapter;
    LanguageAdapter languageAdapter;
    RelativeLayout project_layout,layreshead, layressum, laykey,itskills_layout,education_layout,certification_layout,career_layout,other_detail,language_layout;
    private List<ProfileGS> profileemplo = new ArrayList<ProfileGS>();
    private List<ProfileGS> profileit = new ArrayList<ProfileGS>();
    private List<ProfileGS> profileproject = new ArrayList<ProfileGS>();
    private List<ProfileGS> profileedu = new ArrayList<ProfileGS>();
    private List<ProfileGS> profildesire = new ArrayList<ProfileGS>();
    private List<ProfileGS> profildesire1 = new ArrayList<ProfileGS>();
    private List<ProfileGS> profillang = new ArrayList<ProfileGS>();
    String profilesummaryid;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        pd = new ProgressDialog(Profile.this);
        new Myprofileshow().execute();
        prefManager = new PrefManager(this);
        certification_layout=(RelativeLayout) findViewById(R.id.certification_layout);
        career_layout=(RelativeLayout) findViewById(R.id.career_layout);
        other_detail=(RelativeLayout) findViewById(R.id.other_detail);
        language_layout = (RelativeLayout) findViewById(R.id.language_layout);
        edireshead = (TextView) findViewById(R.id.edireshead);
        education_layout=(RelativeLayout) findViewById(R.id.education_layout);
        itskills_layout=(RelativeLayout) findViewById(R.id.itskill_layout);
        project_layout=(RelativeLayout) findViewById(R.id.project_layout);
        layreshead = (RelativeLayout) findViewById(R.id.layreshead);
        layressum = (RelativeLayout) findViewById(R.id.layressum);
        laykey = (RelativeLayout) findViewById(R.id.laykey);
        employeelist = (RecyclerView) findViewById(R.id.employeelist);
        desirredlist = (RecyclerView) findViewById(R.id.desirredlist);
        otherlist = (RecyclerView) findViewById(R.id.otherlist);
        langlist = (RecyclerView) findViewById(R.id.langlist);
        itlist = (RecyclerView) findViewById(R.id.itlist);
        projctlist = (RecyclerView) findViewById(R.id.projctlist);
        edulist = (RecyclerView) findViewById(R.id.edulist);
        username = (TextView) findViewById(R.id.username);
        employmentlist = (TextView) findViewById(R.id.employmentlist);
        keyskill = (TextView) findViewById(R.id.skill);
        designation = (TextView) findViewById(R.id.designation);
        location = (TextView) findViewById(R.id.location);
        experience = (TextView) findViewById(R.id.experience);
        phno = (TextView) findViewById(R.id.phone_no);
        email = (TextView) findViewById(R.id.email);
        salary = (TextView) findViewById(R.id.salary);
        resume_headline = (TextView) findViewById(R.id.resume_headline);
        profile_summary = (TextView) findViewById(R.id.profilesummary);
        home_profile_image = (CircleImageView) findViewById(R.id.home_profile_image);
        profile_layout = (RelativeLayout) findViewById(R.id.profile_layout);
        edit_button = (ImageView) findViewById(R.id.edit_button);
        employmentlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, WorkExperience.class);
                startActivity(head);
            }
        });
        edireshead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, ResumeHeadline.class);
                head.putExtra("heading", resumeheadline);
                startActivity(head);
            }
        });
        layreshead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, ResumeHeadline.class);
                head.putExtra("heading", resumeheadline);
                startActivity(head);
            }
        });
        layressum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, ProfileSummary.class);

                startActivity(head);
            }
        });
        laykey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, Keyskill.class);

                startActivity(head);

            }
        });
        education_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, Education.class);

                startActivity(head);

            }
        });


        certification_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, Certification.class);
                head.putExtra("summary", profilesummary);
                startActivity(head);
            }
        });
        career_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, DesiredCareer.class);

                startActivity(head);

            }
        });
        other_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, OtherDetails.class);

                startActivity(head);

            }
        });
        language_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, Language.class);

                startActivity(head);

            }
        });

        project_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, Project.class);

                startActivity(head);

            }
        });
        itskills_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent head = new Intent(Profile.this, ITSkills.class);

                startActivity(head);

            }
        });
        resume_headline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Profile.this, Edit_basicprofile.class);
                Edit_basicprofile.username1 = username1;
                Edit_basicprofile.designation1 = designation1;
                Edit_basicprofile.location1 = location1;
                Edit_basicprofile.experience1 = experience1;
                Edit_basicprofile.phno1 = phno1;
                Edit_basicprofile.email1 = email1;
                Edit_basicprofile.salary1 = salary1;
                Edit_basicprofile.userimage1 = userimage1;
                startActivity(i);
            }
        });


    }


    class Myprofileshow extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "allprofiles";
            HttpPost httppost = new HttpPost(url);
            String group_id = "4";
            Intent i = getIntent();

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));

                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);


                JSONArray jsonArray = jsonRootObject.optJSONArray("Profile_details");
                JSONArray jsonArray1 = jsonRootObject.optJSONArray("key_skills_details");
                JSONArray jsonArray2 = jsonRootObject.optJSONArray("employment_details");
                JSONArray jsonArray3 = jsonRootObject.optJSONArray("ITskills_details");
                JSONArray jsonArray4 = jsonRootObject.optJSONArray("Education_details");
                JSONArray jsonArray5 = jsonRootObject.optJSONArray("Languages_details");
                JSONArray jsonArray6 = jsonRootObject.optJSONArray("Project_details");
                JSONArray jsonArray7 = jsonRootObject.optJSONArray("Certification_details");
                JSONArray jsonArray8 = jsonRootObject.optJSONArray("Profile_summary_details");

                for (int i1 = 0; i1 <jsonArray8.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray8.getJSONObject(i1);
                        profilesummary = jsonObject.getString("profile_summary").toString();

                         profilesummaryid=jsonObject.getString("id").toString();



                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
                        String user_id = jsonObject.getString("user_id").toString();
                        username1 = jsonObject.getString("username").toString();
                        email1 = jsonObject.getString("email").toString();
                        phno1 = jsonObject.getString("phone_number").toString();
                        designation1 = jsonObject.getString("designation").toString();
                        location1 = jsonObject.getString("location").toString();
                        experience1 = jsonObject.getString("experience").toString();
                        salary1 = jsonObject.getString("salary").toString();
                        userimage1 = jsonObject.getString("avatar").toString();
                        resumeheadline = jsonObject.getString("resume_headline").toString();

                        industry_type = jsonObject.getString("industry_type").toString();
                        department = jsonObject.getString("department").toString();
                        roles = jsonObject.getString("roles").toString();
                        desired_job_type = jsonObject.getString("desired_job_type").toString();
                        desired_employment_type = jsonObject.getString("desired_employment_type").toString();
                        preferred_location = jsonObject.getString("preferred_location").toString();
                        available_to_join = jsonObject.getString("available_to_join").toString();
                        work_permit_usa = jsonObject.getString("work_permit_usa").toString();
                        work_permit = jsonObject.getString("work_permit").toString();
                        date_of_birth = jsonObject.getString("date_of_birth").toString();
                        gender = jsonObject.getString("gender").toString();
                        home_town = jsonObject.getString("home_town").toString();
                        pincode = jsonObject.getString("pincode").toString();
                        marital_status = jsonObject.getString("marital_status").toString();
                        permanent_address = jsonObject.getString("permanent_address").toString();

                        ProfileGS profileGS = new ProfileGS();

                        ProfileGS profileGS1 = new ProfileGS();

                        profileGS.setIndustry_type(industry_type);
                        profileGS.setDepartment(department);
                        profileGS.setRole(roles);
                        profileGS.setDesired_job_type(desired_job_type);
                        profileGS.setDesired_employment_type(desired_employment_type);
                        profileGS.setPreferred_location(preferred_location);
                        profileGS.setAvailable_to_join(available_to_join);
                        profileGS1.setWork_permit_usa(work_permit_usa);
                        profileGS1.setWork_permit(work_permit);
                        profileGS1.setDate_of_birth(date_of_birth);
                        profileGS1.setGender(gender);
                        profileGS1.setHome_town(home_town);
                        profileGS1.setPincode(pincode);
                        profileGS1.setMarital_status(marital_status);
                        profileGS1.setPermanent_address(permanent_address);
                        profileGS1.setProfile_id(jsonObject.getString("id").toString());
                        profileGS.setProfile_id(jsonObject.getString("id").toString());
                        profildesire.add(profileGS);

                        profildesire1.add(profileGS1);


                        desireAdapter = new DesireAdapter(profildesire);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(Profile.this, LinearLayoutManager.VERTICAL, false);
                                desirredlist.setLayoutManager(horizontalLayoutManagaer);
                                desirredlist.setAdapter(desireAdapter);


                            }
                        });


                        otherAdapter = new OtherAdapter(profildesire1);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(Profile.this, LinearLayoutManager.VERTICAL, false);
                                otherlist.setLayoutManager(horizontalLayoutManagaer);
                                otherlist.setAdapter(otherAdapter);


                            }
                        });


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            username.setText(username1);
                            email.setText(email1);
                            phno.setText(phno1);
                            designation.setText(designation1);
                            location.setText(location1);
                            experience.setText(experience1);
                            salary.setText(salary1);
                            resume_headline.setText(resumeheadline);
                            profile_summary.setText(profilesummary);
                            profile_summary.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i= new Intent(Profile.this,ProfileSummary.class);
                                    ProfileSummary.edit="true";
                                    ProfileSummary.value=profilesummary;
                                    ProfileSummary.id=profilesummaryid;
                                    startActivity(i);
                                }
                            });

                            try {

                                URL url = new URL(userimage1);
                                Glide.with(Profile.this).load(String.valueOf(url)).into(home_profile_image);

                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                for (int i1 = 0; i1 <jsonArray1.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray1.getJSONObject(i1);
                        String key_skills = jsonObject.getString("key_skills").toString();

                        string1.append(key_skills + ",");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                keyskill.setText(string1);
                            }
                        });
                        Log.d("id", "id -----" + key_skills);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                for (int i1 = 0; i1 < jsonArray2.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray2.getJSONObject(i1);
                        String employment_designation = jsonObject.getString("employment_designation").toString();
                        String organization = jsonObject.getString("organization").toString();
                        String current_company = jsonObject.getString("current_company").toString();
                        String started_worked_from = jsonObject.getString("started_worked_from").toString();
                        String worked_till = jsonObject.getString("worked_till").toString();
                        String describe_job_profile = jsonObject.getString("describe_job_profile").toString();
                        String notice_period = jsonObject.getString("notice_period").toString();
                        String id = jsonObject.getString("id").toString();


                        ProfileGS mainPageGS = new ProfileGS();
                        mainPageGS.setEmployment_designation(employment_designation);
                        mainPageGS.setOrganization(organization);
                        mainPageGS.setCurrent_company(current_company);
                        mainPageGS.setStarted_worked_from(started_worked_from);
                        mainPageGS.setWorked_till(worked_till);
                        mainPageGS.setDescribe_job_profile(describe_job_profile);
                        mainPageGS.setNotice_period(notice_period);
                        mainPageGS.setEmp_id(id);
                        profileemplo.add(mainPageGS);

                        profileAdapter = new ProfileAdapter(profileemplo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("employee", "Runnable");

                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(Profile.this, LinearLayoutManager.VERTICAL, false);
                                employeelist.setLayoutManager(horizontalLayoutManagaer);
                                employeelist.setAdapter(profileAdapter);
                            }
                        });


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


                // ITskills_details
                for (int i1 = 0; i1 < jsonArray3.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray3.getJSONObject(i1);
                        String skills_software = jsonObject.getString("skills_software").toString();
                        String version = jsonObject.getString("version").toString();
                        String last_used = jsonObject.getString("last_used").toString();
                        String total_experience = jsonObject.getString("total_experience").toString();
                        String total_months = jsonObject.getString("total_months").toString();
                        String id = jsonObject.getString("id").toString();



                        ProfileGS mainPageGS = new ProfileGS();
                        mainPageGS.setSkills_software(skills_software);
                        mainPageGS.setVersion(version);
                        mainPageGS.setLast_used(last_used);
                        mainPageGS.setTotal_experience(total_experience);
                        mainPageGS.setTotal_months(total_months);
                        mainPageGS.setSkills_id(id);
                        profileit.add(mainPageGS);

                        itAdapter = new ITAdapter(profileit);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(Profile.this, LinearLayoutManager.HORIZONTAL, false);
                                itlist.setLayoutManager(horizontalLayoutManagaer);
                                itlist.setAdapter(itAdapter);
                            }
                        });


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

//                ProjectDetails
                for (int i1 = 0; i1 < jsonArray6.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray6.getJSONObject(i1);
                        String project_title = jsonObject.getString("project_title").toString();
                        String client = jsonObject.getString("client").toString();
                        String started_working_from = jsonObject.getString("started_working_from").toString();
                        String worked_tills = jsonObject.getString("worked_tills").toString();
                        String details_project = jsonObject.getString("details_project").toString();
                        String project_location = jsonObject.getString("project_location").toString();
                        String project_site = jsonObject.getString("project_site").toString();
                        String nature_employment = jsonObject.getString("nature_employment").toString();
                        String team_size = jsonObject.getString("team_size").toString();
                        String role = jsonObject.getString("role").toString();
                        String role_description = jsonObject.getString("role_description").toString();
                        String skills_used = jsonObject.getString("skills_used").toString();
                        String id=jsonObject.getString("id").toString();


                        ProfileGS mainPageGS = new ProfileGS();

                        mainPageGS.setProject_title(project_title);
                        mainPageGS.setProjectclient(client);
                        mainPageGS.setStarted_working_from(started_working_from);
                        mainPageGS.setWorked_tills(worked_tills);
                        mainPageGS.setDetails_project(details_project);
                        mainPageGS.setProject_location(project_location);
                        mainPageGS.setProject_site(project_site);
                        mainPageGS.setNature_employment(nature_employment);
                        mainPageGS.setTeam_size(team_size);
                        mainPageGS.setRole(role);
                        mainPageGS.setRole_description(role_description);
                        mainPageGS.setSkills_used(skills_used);
                        mainPageGS.setProject_id(id);

                        profileproject.add(mainPageGS);


                        projectAdapter = new ProjectAdapter(profileproject);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(Profile.this, LinearLayoutManager.VERTICAL, false);
                                projctlist.setLayoutManager(horizontalLayoutManagaer);
                                projctlist.setAdapter(projectAdapter);

                            }
                        });


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


                // Education_details
                for (int i1 = 0; i1 < jsonArray4.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray4.getJSONObject(i1);

                        String education = jsonObject.getString("education").toString();
                        String course = jsonObject.getString("course").toString();
                        String specialization = jsonObject.getString("specialization").toString();
                        String institute = jsonObject.getString("institute").toString();
                        String year_of_passing = jsonObject.getString("year_of_passing").toString();
                        String time_periods = jsonObject.getString("time_periods").toString();


                        ProfileGS mainPageGS = new ProfileGS();

                        mainPageGS.setEducation(education);
                        mainPageGS.setCourse(course);
                        mainPageGS.setSpecialization(specialization);
                        mainPageGS.setInstitute(institute);
                        mainPageGS.setYear_of_passing(year_of_passing);
                        mainPageGS.setTime_periods(time_periods);
                        mainPageGS.setEdu_id(jsonObject.getString("id").toString());

                        profileedu.add(mainPageGS);


                        eduAdapter = new EDUAdapter(profileedu);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(Profile.this, LinearLayoutManager.VERTICAL, false);

                                edulist.setLayoutManager(horizontalLayoutManagaer);

                                edulist.setAdapter(eduAdapter);

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                // Language_details
                for (int i1 = 0; i1 < jsonArray5.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray5.getJSONObject(i1);

                        String languages = jsonObject.getString("languages").toString();
                        String lan_proficiency = jsonObject.getString("lan_proficiency").toString();
                        String read = jsonObject.getString("read").toString();
                        String write = jsonObject.getString("write").toString();
                        String speak = jsonObject.getString("speak").toString();


                        ProfileGS mainPageGS = new ProfileGS();

                        mainPageGS.setLanguages(languages);
                        mainPageGS.setLan_proficiency(lan_proficiency);
                        mainPageGS.setRead(read);
                        mainPageGS.setWrite(write);
                        mainPageGS.setSpeak(speak);
                        mainPageGS.setLang_id(jsonObject.getString("id").toString());

                        profillang.add(mainPageGS);


                        languageAdapter = new LanguageAdapter(profillang);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                LinearLayoutManager horizontalLayoutManagaer
                                        = new LinearLayoutManager(Profile.this, LinearLayoutManager.VERTICAL, false);
                                langlist.setLayoutManager(horizontalLayoutManagaer);
                                langlist.setAdapter(languageAdapter);

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }

    public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.MyViewHolder> {
        public List<ProfileGS> mainpagelist1;


        public ProfileAdapter(List<ProfileGS> employeelist) {
            this.mainpagelist1 = employeelist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.employmentlist, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final ProfileGS m = mainpagelist1.get(position);
            Log.d("employee", "onBindViewHolder");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Profile.this, WorkExperience.class);


                    WorkExperience.employment_designation=m.getEmployment_designation();
                    WorkExperience.organization3=m.getOrganization();
                    WorkExperience.current_company3=m.getCurrent_company();
                    WorkExperience.started_worked_from3=m.getStarted_worked_from();
                    WorkExperience.worked_till3=m.getWorked_till();
                    WorkExperience.describe_job_profile3=m.getDescribe_job_profile();
                    WorkExperience.notice_period3=m.getAvailable_to_join();
                    WorkExperience.id1=m.getEmp_id();
                    WorkExperience.edit="true";

                    startActivity(intent);
                }
            });

            if (m.getEmployment_designation() != null) {
                if (!m.getEmployment_designation().equals("null")) {

                    holder.employment_designation.setText(m.getEmployment_designation());
                }
            }

            if (m.getOrganization() != null) {
                if (!m.getOrganization().equals("null")) {

                    holder.organization.setText(m.getOrganization());
                }
            }
            if (m.getStarted_worked_from() != null) {
                if (!m.getStarted_worked_from().equals("null")) {

                    holder.started_worked_from.setText(m.getStarted_worked_from() + " to " + m.getWorked_till());
                }
            }

            if (m.getNotice_period() != null) {
                if (!m.getNotice_period().equals("null")) {

                    holder.notice_period.setText("Available to join "+m.getNotice_period());
                }
            }

            if (m.getDescribe_job_profile() != null) {
                if (!m.getDescribe_job_profile().equals("null")) {

                    holder.describe_job_profile.setText(m.getDescribe_job_profile());
                }
            }
        }

        @Override
        public int getItemViewType(int position) {

            return position;

        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return mainpagelist1.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView employment_designation, organization, current_company, started_worked_from, worked_till, describe_job_profile, notice_period;

            public MyViewHolder(View convertView) {
                super(convertView);

                employment_designation = (TextView) convertView.findViewById(R.id.employment_designation);
                organization = (TextView) convertView.findViewById(R.id.organization);
                started_worked_from = (TextView) convertView.findViewById(R.id.started_worked_from);
                describe_job_profile = (TextView) convertView.findViewById(R.id.describe_job_profile);
                notice_period = (TextView) convertView.findViewById(R.id.notice_period);
            }
        }


    }

    public class ITAdapter extends RecyclerView.Adapter<ITAdapter.MyViewHolder> {
        public List<ProfileGS> mainpagelist2;
        Context mcontext;
        int globalInc = 0;


        public ITAdapter(Context mcontext, List<ProfileGS> mainpagelist1) {

            this.mcontext = mcontext;
            this.mainpagelist2 = mainpagelist1;
        }

        public ITAdapter(List<ProfileGS> employeelist) {
            this.mainpagelist2 = employeelist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.itlist, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final ProfileGS m = mainpagelist2.get(position);

            Log.d("itskills", "onBindViewHolder");


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Profile.this, ITSkills.class);



                    ITSkills.Skills_software=m.getSkills_software();
                    ITSkills.Version=m.getVersion();
                    ITSkills.Last_used=m.getLast_used();
                    ITSkills.Total_experience=m.getTotal_experience();
                    ITSkills.Total_months=m.getTotal_months();
                    ITSkills.Skills_id=m.getSkills_id();

                    ITSkills.edit="true";



                    startActivity(intent);
                }
            });


            if (m.getSkills_software() != null) {
                if (!m.getSkills_software().equals("null")) {

                    holder.itname.setText(m.getSkills_software() + " " + "(" + m.getVersion() + ")");
                }
            }

            if (m.getLast_used() != null) {
                if (!m.getLast_used().equals("null")) {

                    holder.ityear.setText(m.getTotal_experience() + " YEAR" + m.getTotal_months() + " MONTHS | " + m.getLast_used());
                }
            }
        }

        @Override
        public int getItemViewType(int position) {

            return position;

        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return mainpagelist2.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView itname, ityear;

            public MyViewHolder(View convertView) {
                super(convertView);

                itname = (TextView) convertView.findViewById(R.id.itname);
                ityear = (TextView) convertView.findViewById(R.id.ityear);

            }
        }


    }

    public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.MyViewHolder> {
        public List<ProfileGS> mainpagelist2;
        Context mcontext;
        int globalInc = 0;


        public ProjectAdapter(List<ProfileGS> employeelist) {
            this.mainpagelist2 = employeelist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.projectlist, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final ProfileGS m = mainpagelist2.get(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Profile.this, Project.class);

                    Project.Project_title=m.getProject_title();
                    Project.Projectclient=m.getProjectclient();
                    Project.Started_working_from=m.getStarted_working_from();
                    Project.Worked_tills=m.getWorked_tills();
                    Project.Details_project=m.getDetails_project();
                    Project.Project_location=m.getProject_location();
                    Project.Project_site=m.getProject_site();
                    Project.Nature_employment=m.getNature_employment();
                    Project.Team_size=m.getTeam_size();
                    Project.Role=m.getRole();
                    Project.Role_description=m.getRole_description();
                    Project.Skills_used=m.getSkills_used();
                    Project.id=m.getProject_id();
                    Project.edit="true";



                    startActivity(intent);
                }
            });

            if (m.getProject_title() != null) {
                if (!m.getProject_title().equals("null")) {

                    holder.projecttitle.setText(m.getProject_title());
                }
            }

            if (m.getProjectclient() != null) {
                if (!m.getProjectclient().equals("null")) {

                    holder.projectclient.setText(m.getProjectclient() + " " + "(" + m.getProject_site() + ")");
                }
            }
            if (m.getStarted_working_from() != null) {
                if (!m.getStarted_working_from().equals("null")) {

                    holder.projecttime.setText(m.getStarted_working_from() + " " + m.getWorked_tills() + " " + m.getNature_employment());
                }
            }
            if (m.getRole_description() != null) {
                if (!m.getRole_description().equals("null")) {

                    holder.projectdesignation.setText(m.getRole_description());
                }
            }
        }

        @Override
        public int getItemViewType(int position) {

            return position;

        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return mainpagelist2.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView projecttitle, projectclient, projecttime, projectdesignation;

            public MyViewHolder(View convertView) {
                super(convertView);

                projecttitle = (TextView) convertView.findViewById(R.id.projectname);
                projectclient = (TextView) convertView.findViewById(R.id.projectclient);
                projecttime = (TextView) convertView.findViewById(R.id.projecydate);
                projectdesignation = (TextView) convertView.findViewById(R.id.projectrole);

            }
        }


    }

    public class EDUAdapter extends RecyclerView.Adapter<EDUAdapter.MyViewHolder> {
        public List<ProfileGS> mainpagelist2;
        Context mcontext;
        int globalInc = 0;


        public EDUAdapter(List<ProfileGS> employeelist) {
            this.mainpagelist2 = employeelist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.educationlist, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final ProfileGS m = mainpagelist2.get(position);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Profile.this, Education.class);

                    Education.Education=m.getEducation();
                    Education.Course=m.getCourse();
                    Education.Specialization=m.getSpecialization();
                    Education.Institute=m.getInstitute();
                    Education.Year_of_passing=m.getYear_of_passing();
                    Education.Time_periods=m.getTime_periods();
                    Education.id=m.getEdu_id();

                    Education.edit="true";



                    startActivity(intent);
                }
            });

            if (m.getCourse() != null) {
                if (!m.getCourse().equals("null")) {

                    holder.educourse.setText(m.getCourse() + " " + m.getSpecialization());
                }
            }

            if (m.getInstitute() != null) {
                if (!m.getInstitute().equals("null")) {

                    holder.eduuniver.setText(m.getInstitute());
                }
            }
            if (m.getYear_of_passing() != null) {
                if (!m.getYear_of_passing().equals("null")) {

                    holder.eduyear.setText(m.getYear_of_passing() + "," + m.getTime_periods());
                }
            }

        }

        @Override
        public int getItemViewType(int position) {

            return position;

        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return mainpagelist2.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView educourse, eduuniver, eduyear;

            public MyViewHolder(View convertView) {
                super(convertView);

                educourse = (TextView) convertView.findViewById(R.id.educourse);
                eduuniver = (TextView) convertView.findViewById(R.id.eduuniversity);
                eduyear = (TextView) convertView.findViewById(R.id.eduyear);


            }
        }


    }

    public class DesireAdapter extends RecyclerView.Adapter<DesireAdapter.MyViewHolder> {
        public List<ProfileGS> mainpagelist2;
        Context mcontext;
        int globalInc = 0;


        public DesireAdapter(List<ProfileGS> employeelist) {
            this.mainpagelist2 = employeelist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.desiredlist, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final ProfileGS m = mainpagelist2.get(position);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Profile.this, DesiredCareer.class);


                    DesiredCareer.Industry_type=m.getIndustry_type();
                    DesiredCareer.Department=m.getDepartment();
                    DesiredCareer.Role=m.getRole();


                    DesiredCareer.Desired_job_type=m.getDesired_job_type();
                    DesiredCareer.Desired_employment_type=m.getDesired_employment_type();

                    DesiredCareer.Preferred_location=m.getPreferred_location();
                    DesiredCareer.Work_permit_usa=m.getWork_permit_usa();
                    DesiredCareer.Work_permit=m.getWork_permit();
                    DesiredCareer.id=m.getProfile_id();

                    DesiredCareer.edit="true";

/*
                    DesiredCareer.Available_to_join=m.getAvailable_to_join();

                    DesiredCareer.Date_of_birth=m.getDate_of_birth();
                    DesiredCareer.Gender=m.getGender();
                    DesiredCareer.Home_town=m.getHome_town();
                    DesiredCareer.Pincode=m.getPincode();
                    DesiredCareer.Marital_status=m.getMarital_status();
                    DesiredCareer.Permanent_address=m.getPermanent_address();*/






                    startActivity(intent);
                }
            });
            if (m.getRole() != null) {
                if (!m.getRole().equals("null")) {

                    holder.role1.setText(m.getRole());
                }
            }

            if (m.getDepartment() != null) {
                if (!m.getDepartment().equals("null")) {

                    holder.function1.setText(m.getDepartment());
                }
            }
            if (m.getIndustry_type() != null) {
                if (!m.getIndustry_type().equals("null")) {

                    holder.industry1.setText(m.getIndustry_type());
                }
            }
            if (m.getDesired_job_type() != null) {
                if (!m.getDesired_job_type().equals("null")) {

                    holder.jobtype1.setText(m.getDesired_job_type());
                }
            }
            if (m.getDesired_employment_type() != null) {
                if (!m.getDesired_employment_type().equals("null")) {

                    holder.employment1.setText(m.getDesired_employment_type());
                }
            }
            if (m.getPreferred_location() != null) {
                if (!m.getPreferred_location().equals("null")) {

                    holder.location1.setText(m.getPreferred_location());
                }
            }
        }

        @Override
        public int getItemViewType(int position) {

            return position;

        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return mainpagelist2.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView role1, function1, industry1, jobtype1, employment1, location1;

            public MyViewHolder(View convertView) {
                super(convertView);

                role1 = (TextView) convertView.findViewById(R.id.role1);
                function1 = (TextView) convertView.findViewById(R.id.function1);
                industry1 = (TextView) convertView.findViewById(R.id.industry1);
                jobtype1 = (TextView) convertView.findViewById(R.id.jobtype1);
                employment1 = (TextView) convertView.findViewById(R.id.employment1);
                location1 = (TextView) convertView.findViewById(R.id.location1);


            }
        }


    }

    public class OtherAdapter extends RecyclerView.Adapter<OtherAdapter.MyViewHolder> {
        public List<ProfileGS> mainpagelist2;
        Context mcontext;
        int globalInc = 0;


        public OtherAdapter(List<ProfileGS> employeelist) {
            this.mainpagelist2 = employeelist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.otherlist, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final ProfileGS m = mainpagelist2.get(position);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
            Intent intent = new Intent(Profile.this, OtherDetails.class);



            OtherDetails.id=m.getProfile_id();

            OtherDetails.edit="true";

            OtherDetails.Available_to_join=m.getAvailable_to_join();

            OtherDetails.Date_of_birth=m.getDate_of_birth();
            OtherDetails.Gender=m.getGender();
            OtherDetails.Home_town=m.getHome_town();
            OtherDetails.Pincode=m.getPincode();
            OtherDetails.Marital_status=m.getMarital_status();
            OtherDetails.Permanent_address=m.getPermanent_address();






            startActivity(intent);}});

            if (m.getDate_of_birth() != null) {
                if (!m.getDate_of_birth().equals("null")) {

                    holder.dob.setText(m.getDate_of_birth());
                }
            }

            if (m.getGender() != null) {
                if (!m.getGender().equals("null")) {

                    holder.gender.setText(m.getGender());
                }
            }
            if (m.getHome_town() != null) {
                if (!m.getHome_town().equals("null")) {

                    holder.hometown.setText(m.getHome_town());
                }
            }
            if (m.getPincode() != null) {
                if (!m.getPincode().equals("null")) {

                    holder.areacode.setText(m.getPincode());
                }
            }
            if (m.getPermanent_address() != null) {
                if (!m.getPermanent_address().equals("null")) {

                    holder.permanent.setText(m.getPermanent_address());
                }
            }
            if (m.getWork_permit() != null) {
                if (!m.getWork_permit().equals("null")) {

                    holder.work.setText(m.getWork_permit());
                }
            }
        }

        @Override
        public int getItemViewType(int position) {

            return position;

        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return mainpagelist2.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView dob, gender, hometown, areacode, permanent, work;

            public MyViewHolder(View convertView) {
                super(convertView);

                dob = (TextView) convertView.findViewById(R.id.dob1);
                gender = (TextView) convertView.findViewById(R.id.gen1);
                hometown = (TextView) convertView.findViewById(R.id.home1);
                areacode = (TextView) convertView.findViewById(R.id.area1);
                permanent = (TextView) convertView.findViewById(R.id.per1);
                work = (TextView) convertView.findViewById(R.id.wor1);


            }
        }


    }

    public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {
        public List<ProfileGS> mainpagelist2;
        Context mcontext;
        int globalInc = 0;


        public LanguageAdapter(List<ProfileGS> employeelist) {
            this.mainpagelist2 = employeelist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.langlist, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final ProfileGS m = mainpagelist2.get(position);




            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Profile.this, Language.class);

                    Language.Languages=m.getLanguages();
                    Language.Lan_proficiency=m.getLan_proficiency();
                    Language.Read=m.getRead();
                    Language.Write=m.getWrite();
                    Language.Speak=m.getSpeak();
                    Language.id=m.getLang_id();
                    Language.edit="true";

                    startActivity(intent);
                }
            });

            if (m.getLanguages() != null) {
                if (!m.getLanguages().equals("null")) {

                    holder.langname.setText(m.getLanguages() + " " + m.getLan_proficiency());
                }
            }

            if (m.getRead() != null) {
                if (!m.getRead().equals("null")) {
                    String read = m.getRead();
                    String write = m.getWrite();
                    String speak = m.getSpeak();

                    if (read.equals("1")) {
                        holder.langread.setText(" read ");
                    }
                    if (write.equals("1")) {
                        holder.langwrite.setText(" write ");
                    }
                    if (speak.equals("1")) {
                        holder.langspeak.setText(" speak ");
                    }

                }
            }

        }

        @Override
        public int getItemViewType(int position) {

            return position;

        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return mainpagelist2.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView langname, langread, langwrite, langspeak;

            public MyViewHolder(View convertView) {
                super(convertView);

                langname = (TextView) convertView.findViewById(R.id.lanname);
                langread = (TextView) convertView.findViewById(R.id.langread);
                langwrite = (TextView) convertView.findViewById(R.id.langwrite);
                langspeak = (TextView) convertView.findViewById(R.id.langspeak);


            }
        }


    }
}
