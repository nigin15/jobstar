package com.abservetech.jobstar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Locale;

/**
 * Created by user on 9/21/2016.
 */
public class LocalizationUpdaterActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeLocale(LocalizationUpdaterActivity.this,"th");
        this.setContentView(R.layout.language);

        Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        Button button=(Button)findViewById(R.id.button1) ;



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(LocalizationUpdaterActivity.this,HomePage.class);
                startActivity(i);
            }
        });
        spinner.setPrompt("select language");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,languages);

/*        ArrayAdapter<string> adapter = new ArrayAdapter<string>(this,
                android.R.layout.simple_spinner_item, languages);*/
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView arg0, View arg1,
                                       int arg2, long arg3) {
                Configuration config = new Configuration();
                switch (arg2) {
                    case 0:
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(LocalizationUpdaterActivity.this);
                        preferences.edit().putString("lang", "ar").commit();
                        break;
                    case 1:

                        break;

                    default:
                        config.locale = Locale.TAIWAN;
                        break;
                }

            }

            public void onNothingSelected(AdapterView arg0) {
                // TODO Auto-generated method stub

            }
        });

    }
    private void updateViews() {
        // if you want you just call activity to restart itself to redraw all the widgets with the correct locale
        // however, it will cause a bad look and feel for your users
        //
        // this.recreate();

        //or you can just update the visible text on your current layout
        Resources resources = getResources();
        TextView textView10=(TextView)findViewById(R.id.textView10);
        textView10.setText(resources.getString(R.string.title_text));

    }

    public static void setDefaultLocale(Context context, String locale) {
        Locale locJa = new Locale(locale.trim());
        Locale.setDefault(locJa);

        Configuration config = new Configuration();
        config.locale = locJa;

        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());

        locJa = null;
        config = null;
    }

    private String[] languages = { "Inglese", "Italiano", "Francese" };

    public static void changeLocale(Activity activity, String language)
    {
        final Resources res = activity.getResources();
        final Configuration conf = res.getConfiguration();
        if (language == null || language.length() == 0)
        {
            conf.locale = Locale.getDefault();
        }
        else
        {
            final int idx = language.indexOf('-');
            if (idx != -1)
            {
                final String[] split = language.split("-");
                conf.locale = new Locale(split[0], split[1].substring(1));
            }
            else
            {
                conf.locale = new Locale(language);
            }
        }

        res.updateConfiguration(conf, null);
    }
}
