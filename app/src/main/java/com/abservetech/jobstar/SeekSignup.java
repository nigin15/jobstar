package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SeekSignup extends AppCompatActivity {
    Button signupsubmit;
    String sendemail, sendpassword, sendphone, sendname;
    EditText email, password, phone, first_name;
    RelativeLayout coordinatorLayout;
    ProgressDialog pd;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

//Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_signup);
        signupsubmit = (Button) findViewById(R.id.sign_up);
        email = (EditText) findViewById(R.id.sig_email);
        password = (EditText) findViewById(R.id.sig_password);
        phone = (EditText) findViewById(R.id.sig_phone);
        first_name = (EditText) findViewById(R.id.sig_user);
        pd = new ProgressDialog(SeekSignup.this);
        prefManager = new PrefManager(this);
        coordinatorLayout = (RelativeLayout) findViewById(R.id.coordinator1);
        signupsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendemail = email.getText().toString();
                sendpassword = password.getText().toString();
                sendphone = phone.getText().toString();
                sendname = first_name.getText().toString();
                validation(sendemail, sendpassword, sendphone, sendname);


            }
        });
    }
    private void validation(String sendemail, String sendpassword, String sendphone, String sendname) {
        if (this.sendname.length() > 0) {
            if (this.sendname.length() >= 4) {


                //Toast.makeText(this, " valid email", Toast.LENGTH_SHORT).show();
                if (this.sendpassword.length() > 0) {

                    if (this.sendpassword.length() >= 6 && this.sendpassword.length() <= 12) {
                        if (Patterns.EMAIL_ADDRESS.matcher(this.sendemail).matches()) {
                            if (this.sendphone.length() >= 8) {
//                                new AsyncT1().execute(sendemail, sendpassword, sendphone, sendname,regid);
                                new AsyncT1().execute(sendemail, sendpassword, sendphone, sendname);


                            } else {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout,"Enter valid Mobile number", Snackbar.LENGTH_LONG);
                                View snackbarView = snackbar.getView();
                                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                                textView.setTextColor(Color.RED);
                                snackbarView.setBackgroundColor(Color.WHITE);
                                snackbar.show();
                                phone.setBackgroundResource(R.drawable.rounder_whiteedit);

                                phone.setError("Enter valid Mobile number");
                            }
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Enter valid Email", Snackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(Color.RED);
                            snackbarView.setBackgroundColor(Color.WHITE);
                            snackbar.show();
                            email.setBackgroundResource(R.drawable.rounder_whiteedit);
                            email.setError("Enter valid Email");

                        }
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "Password must be 6-12", Snackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.RED);
                        snackbarView.setBackgroundColor(Color.WHITE);
                        snackbar.show();
                        password.setBackgroundResource(R.drawable.rounder_whiteedit);
                        password.setError("Password must be 6-12");
                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Enter password", Snackbar.LENGTH_LONG);
                    View snackbarView = snackbar.getView();
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.RED);
                    snackbarView.setBackgroundColor(Color.WHITE);
                    snackbar.show();
                    password.setBackgroundResource(R.drawable.rounder_whiteedit);


                    password.setError("Enter password");
                }

            } else {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Must be atleast 4 characters", Snackbar.LENGTH_LONG);
                View snackbarView = snackbar.getView();
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.RED);
                snackbarView.setBackgroundColor(Color.WHITE);
                snackbar.show();
                first_name.setBackgroundResource(R.drawable.rounder_whiteedit);

                first_name.setError("Must be atleast 4 characters");
            }
        } else {
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Enter username", Snackbar.LENGTH_LONG);
            View snackbarView = snackbar.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.RED);
            snackbarView.setBackgroundColor(Color.WHITE);
            snackbar.show();
            first_name.setBackgroundResource(R.drawable.rounder_whiteedit);
            first_name.setError("Enter username");
        }
    }
    class AsyncT1 extends AsyncTask<String, Void, String> {
        String first;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = "http://www.abservetechdemo.com/projects/client/jobberman/public/mobile/user/create";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("email", sendemail));
                nameValuePairs.add(new BasicNameValuePair("username", sendname));
                nameValuePairs.add(new BasicNameValuePair("password", sendpassword));
                nameValuePairs.add(new BasicNameValuePair("phone_number", sendphone));
               // nameValuePairs.add(new BasicNameValuePair("regid", regid));
                 nameValuePairs.add(new BasicNameValuePair("group_id", "2"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);

                first = jsonRootObject.getString("id");



                if (first.equals("5")) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(SeekSignup.this, "Enter another E-mail id", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                final String user_id = jsonRootObject.getString("insert_id");
                final String acti = jsonRootObject.getString("message");
                if (first.equals("1")) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            prefManager.setuserid(user_id);
                            //  prefManager.setactid(acti);
                            // Intent bLogin = new Intent(SeekSignup.this, Home.class);
                            //  prefManager.setFirstTimeLogin(false);
                            // startActivity(bLogin);

                            Toast.makeText(SeekSignup.this, acti, Toast.LENGTH_SHORT).show();
                            String id=prefManager.getuserid();
                            Log.d("id----",id);
                        }
                    });

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();





        }
    }

}
