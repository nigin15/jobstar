package com.abservetech.jobstar;

/**
 * Created by user on 9/14/2016.
 */
public class HomepageGS {
    String id,user_id,job_title,company_name,job_experience,job_location,job_specialization,job_date,job_id,job_Applied,job_Saved;

    String savedjob_title,savedcompany_name,savedjob_experience,savedjob_location,savedjob_specialization,savedjob_Applied,savedjob_Saved,savedjob_date,savedjob_id;

    String recruiterjob_title,recruitercompany_name,recruiterjob_experience,recruiterjob_Applied,recruiterjob_Saved,recruiterjob_specialization,recruiterjob_date,recruiterjob_location,recruiterjob_id;

    String applyjob_title,applyjob_id,applycompany_name,applyjob_experience,applyjob_location,applyjob_specialization,applyjob_Saved,applyjob_Applied,applyjob_date;

    String searchjob_Saved,searchjob_Applied,searchjob_title,searchcompany_name,searchjob_experience,searchjob_location,searchjob_specialization,searchjob_date,searchjob_id;

    String apply1job_title,apply1job_id,apply1company_name,apply1job_experience,apply1job_location,apply1job_specialization,apply1job_Saved,apply1job_Applied,apply1job_date;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setjob_title(String job_title) {
        this.job_title = job_title;
    }

    public void setcompany_name(String company_name) {
        this.company_name = company_name;
    }

    public void setjob_experience(String job_experience) {
        this.job_experience = job_experience;
    }

    public void setjob_location(String job_location) {
        this.job_location = job_location;
    }

    public void setjob_specialization(String job_specialization) {
        this.job_specialization = job_specialization;
    }

    public String getjob_title() {
        return job_title;
    }

    public String getcompany_name() {
        return company_name;
    }

    public String getjob_experience() {
        return job_experience;
    }

    public String getjob_location() {
        return job_location;
    }

    public String getjob_specialization() {
        return job_specialization;
    }


    public void setsavedjob_title(String savedjob_title) {
        this.savedjob_title = savedjob_title;
    }



    public void setsavedcompany_name(String savedcompany_name) {
        this.savedcompany_name = savedcompany_name;
    }

    public void setsavedjob_experience(String savedjob_experience) {
        this.savedjob_experience = savedjob_experience;
    }

    public void setsavedjob_location(String savedjob_location) {
        this.savedjob_location = savedjob_location;
    }

    public void setsavedjob_specialization(String savedjob_specialization) {
        this.savedjob_specialization = savedjob_specialization;
    }

    public String getsavedjob_title() {
        return savedjob_title;
    }

    public String getsavedcompany_name() {
        return savedcompany_name;
    }

    public String getsavedjob_experience() {
        return savedjob_experience;
    }

    public String getsavedjob_location() {
        return savedjob_location;
    }

    public String getsavedjob_specialization() {
        return savedjob_specialization;
    }





    public void setrecruiterjob_title(String recruiterjob_title) {
        this.recruiterjob_title = recruiterjob_title;
    }

    public void setrecruitercompany_name(String recruitercompany_name) {
        this.recruitercompany_name = recruitercompany_name;
    }

    public void setrecruiterjob_experience(String recruiterjob_experience) {
        this.recruiterjob_experience = recruiterjob_experience;
    }

    public void setrecruiterjob_location(String recruiterjob_location) {
        this.recruiterjob_location = recruiterjob_location;
    }

    public void setrecruiterjob_specialization(String recruiterjob_specialization) {
        this.recruiterjob_specialization = recruiterjob_specialization;
    }

    public String getrecruiterjob_title() {
        return recruiterjob_title;
    }

    public String getrecruitercompany_name() {
        return recruitercompany_name;
    }

    public String getrecruiterjob_experience() {
        return recruiterjob_experience;
    }

    public String getrecruiterjob_location() {
        return recruiterjob_location;
    }

    public String getrecruiterjob_specialization() {
        return recruiterjob_specialization;
    }



    public void setapplyjob_title(String applyjob_title) {
        this.applyjob_title = applyjob_title;
    }

    public void setapplycompany_name(String applycompany_name) {
        this.applycompany_name = applycompany_name;
    }

    public void setapplyjob_experience(String applyjob_experience) {
        this.applyjob_experience = applyjob_experience;
    }

    public void setapplyjob_location(String applyjob_location) {
        this.applyjob_location = applyjob_location;
    }

    public void setapplyjob_specialization(String applyjob_specialization) {
        this.applyjob_specialization = applyjob_specialization;
    }

    public String getapplyjob_title() {
        return applyjob_title;
    }

    public String getapplycompany_name() {
        return applycompany_name;
    }

    public String getapplyjob_experience() {
        return applyjob_experience;
    }

    public String getapplyjob_location() {
        return applyjob_location;
    }

    public String getapplyjob_specialization() {
        return applyjob_specialization;
    }

    public void setjob_date(String job_date) {
        this.job_date = job_date;
    }

    public void setsavedjob_date(String savedjob_date) {
        this.savedjob_date = savedjob_date;
    }

    public void setrecruiterjob_date(String recruiterjob_date) {
        this.recruiterjob_date = recruiterjob_date;
    }

    public void setapplyjob_date(String applyjob_date) {
        this.applyjob_date = applyjob_date;
    }

    public String getrecruiterjob_date() {
        return recruiterjob_date;
    }

    public String getapplyjob_date() {
        return applyjob_date;
    }

    public String getsavedjob_date() {
        return savedjob_date;
    }

    public String getjob_date() {
        return job_date;
    }

    public void setjob_id(String job_id) {
        this.job_id = job_id;
    }

    public void setsavedjob_id(String savedjob_id) {
        this.savedjob_id = savedjob_id;
    }

    public void setrecruiterjob_id(String recruiterjob_id) {
        this.recruiterjob_id = recruiterjob_id;
    }

    public void setapplyjob_id(String applyjob_id) {
        this.applyjob_id = applyjob_id;
    }

    public String getjob_id() {
        return job_id;
    }

    public String getsavedjob_id() {
        return savedjob_id;
    }

    public String getrecruiterjob_id() {
        return recruiterjob_id;
    }

    public String getapplyjob_id() {
        return applyjob_id;
    }




    public void setsearchjob_title(String searchjob_title) {
        this.searchjob_title = searchjob_title;
    }

    public void setsearchcompany_name(String searchcompany_name) {
        this.searchcompany_name = searchcompany_name;
    }

    public void setsearchjob_experience(String searchjob_experience) {
        this.searchjob_experience = searchjob_experience;
    }

    public void setsearchjob_location(String searchjob_location) {
        this.searchjob_location = searchjob_location;
    }

    public void setsearchjob_specialization(String searchjob_specialization) {
        this.searchjob_specialization = searchjob_specialization;
    }

    public void setsearchjob_date(String searchjob_date) {
        this.searchjob_date = searchjob_date;
    }

    public void setsearchjob_id(String searchjob_id) {
        this.searchjob_id = searchjob_id;
    }

    public String getsearchjob_title() {
        return searchjob_title;
    }

    public String getsearchjob_date() {
        return searchjob_date;
    }

    public String getsearchcompany_name() {
        return searchcompany_name;
    }

    public String getsearchjob_experience() {
        return searchjob_experience;
    }

    public String getsearchjob_location() {
        return searchjob_location;
    }

    public String getsearchjob_specialization() {
        return searchjob_specialization;
    }

    public String getsearchjob_id() {
        return searchjob_id;
    }

    public void setsearchjob_Saved(String searchjob_Saved) {
        this.searchjob_Saved = searchjob_Saved;
    }

    public void setsearchjob_Applied(String searchjob_Applied) {
        this.searchjob_Applied = searchjob_Applied;
    }

    public String getsearchjob_Applied() {
        return searchjob_Applied;
    }

    public String getsearchjob_Saved() {
        return searchjob_Saved;
    }



    public void setapply1job_title(String apply1job_title) {
        this.apply1job_title = apply1job_title;
    }

    public void setapply1company_name(String apply1company_name) {
        this.apply1company_name = apply1company_name;
    }

    public void setapply1job_experience(String apply1job_experience) {
        this.apply1job_experience = apply1job_experience;
    }

    public void setapply1job_location(String apply1job_location) {
        this.apply1job_location = apply1job_location;
    }

    public void setapply1job_specialization(String apply1job_specialization) {
        this.apply1job_specialization = apply1job_specialization;
    }

    public String getapply1job_title() {
        return apply1job_title;
    }

    public String getapply1company_name() {
        return apply1company_name;
    }

    public String getapply1job_experience() {
        return apply1job_experience;
    }

    public String getapply1job_location() {
        return apply1job_location;
    }

    public String getapply1job_specialization() {
        return apply1job_specialization;
    }

    public void setapply1job_date(String apply1job_date) {
        this.apply1job_date = apply1job_date;
    }

    public void setapply1job_id(String apply1job_id) {
        this.apply1job_id = apply1job_id;
    }

    public String getapply1job_id() {
        return apply1job_id;
    }

    public String getapply1job_date() {
        return apply1job_date;
    }
}
