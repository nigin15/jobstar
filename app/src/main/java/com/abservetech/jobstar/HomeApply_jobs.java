package com.abservetech.jobstar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/14/2016.
 */
public class HomeApply_jobs extends Activity{

    public static String count;
    ImageView back;
    ApplyAdapter adapter;
    ListView recommended_listView;
    TextView Chckin;
    public  static   List<HomepageGS> movieList = new ArrayList<HomepageGS>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reccomended_job);

        back=(ImageView)findViewById(R.id.back);
        recommended_listView=(ListView)findViewById(R.id.recommended_listView);
        Chckin=(TextView)findViewById(R.id.Chckin);
        Chckin.setText("Applied Jobs");
        TextView emptyview=(TextView)findViewById(R.id.emptyview);
        if(count.equals("0")){
            emptyview.setVisibility(View.VISIBLE);
        }
        else{
            emptyview.setVisibility(View.GONE);
        }

        adapter=new ApplyAdapter(HomeApply_jobs.this, movieList);
        recommended_listView.setAdapter(adapter);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }




    public class ApplyAdapter extends BaseAdapter {

        private List<HomepageGS> movieItems;
        HomeApply_jobs activity;


        public ApplyAdapter(HomeApply_jobs activity, List<HomepageGS> movieItems) {
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
//        userid = sharedPreferences.getString("userid",null);
//        currency1 = sharedPreferences.getString("currenycode",null);
//        System.out.println("userid in shared preferences in custom search adapter"+userid);

            this.activity = activity;
            this.movieItems = movieItems;
        }

        class ViewHolder {

            protected TextView date,designation,company,experience,location,skill;
            protected CheckBox wishlist;
            ImageView thumbNail;

        }
        @Override
        public int getCount() {
            return movieItems.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                LayoutInflater inflator = LayoutInflater.from(parent.getContext());
                convertView = inflator.inflate(R.layout.job_list1, null);
                viewHolder = new ViewHolder();
                viewHolder. designation=(TextView)convertView.findViewById(R.id.textView5);
                viewHolder. company=(TextView)convertView.findViewById(R.id.textView7);
                viewHolder. experience=(TextView)convertView.findViewById(R.id.textView9);
                viewHolder. location=(TextView)convertView.findViewById(R.id.textView48);
                viewHolder. skill=(TextView)convertView.findViewById(R.id.textView56);
                viewHolder.date=(TextView)convertView.findViewById(R.id.date);

                convertView.setTag(viewHolder);

            }else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final HomepageGS m = movieItems.get(position);
            Log.d("job_title---",m.getapplyjob_title());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(!count.equals("0")){
                        Intent i= new Intent(activity,Job_detail.class);
                        Job_detail.id=m.getapplyjob_id();
                        startActivity(i);}
                    else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                             //   Toast.makeText(activity,"Job not applied",Toast.LENGTH_SHORT).show();



                            }
                        });
                    }

                }
            });
            if (m.getapplyjob_title()!=null)
            {
                if (!m.getapplyjob_title().equals("null"))
                {
                    viewHolder. designation.setText(m.getapplyjob_title());
                }
            }

            if (m.getapplyjob_date()!=null)
            {
                if (!m.getapplyjob_date().equals("null"))
                {

                    viewHolder. date.setText("Posted on "+m.getapplyjob_date());
                }
            }
            if (m.getapplycompany_name()!=null)
            {
                if (!m.getapplycompany_name().equals("null"))
                {
                    viewHolder. company.setText(m.getapplycompany_name());
                }
            }


            if (m.getapplyjob_experience()!=null)
            {
                if (!m.getapplyjob_experience().equals("null"))
                {
                    viewHolder. experience.setText(m.getapplyjob_experience());
                }
            }

            if (m.getapplyjob_location()!=null)
            {
                if (!m.getapplyjob_location().equals("null"))
                {
                    viewHolder. location.setText(m.getapplyjob_location());
                }
            }

            if (m.getapplyjob_specialization()!=null)
            {
                if (!m.getapplyjob_specialization().equals("null"))
                {
                    viewHolder. skill.setText(m.getapplyjob_specialization());
                }
            }

            return convertView;
        }
    }
}
