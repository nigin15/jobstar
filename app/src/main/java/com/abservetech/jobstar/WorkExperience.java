package com.abservetech.jobstar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WorkExperience extends AppCompatActivity {

    public static String employment_designation;
    public static String organization3;
    public static String current_company3;
    public static String started_worked_from3;
    public static String worked_till3;
    public static String describe_job_profile3;
    public static String notice_period3;
    public static String edit;
    public static String id1;
    EditText workedstill,workingfrom, notice, designation,organization,jobprofile;
    RadioButton radio,currentyes, currentno;
    FrameLayout chumma;
    ProgressDialog pd;
    Button save;
    String current_company,workedstill1,workingfrom1, notice1, designation1,organization1,jobprofile1;
    RadioGroup radioGroup;
    PrefManager prefManager;
    MenuItem register;
    String employment_designation1,organization2,current_company1,started_worked_from1,worked_till1,notice_period1,describe_job_profile1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_experience);
        pd = new ProgressDialog(WorkExperience.this);

        prefManager = new PrefManager(this);
        designation= (EditText) findViewById(R.id.designation);
        organization= (EditText) findViewById(R.id.organization);

        workedstill = (EditText) findViewById(R.id.workingtill);
        workingfrom = (EditText) findViewById(R.id.workingfrom);
        jobprofile= (EditText) findViewById(R.id.jobprofile);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        currentyes = (RadioButton) findViewById(R.id.currentyes);
        currentno = (RadioButton) findViewById(R.id.currentno);
        notice = (EditText) findViewById(R.id.notice);
        chumma = (FrameLayout) findViewById(R.id.chumma);
        save =(Button)findViewById(R.id.button2);
       // new Work_experienceshow().execute();


        workingfrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "timePicker");
            }
        });

        designation.setText(employment_designation);
        organization.setText(organization3);
        if(current_company3!=null){
        if(current_company3.equals("Yes")){
            currentyes.setChecked(true);
        }
        if(current_company3.equals("No")){
            currentno.setChecked(true);
        }}
        workingfrom.setText(started_worked_from3);
        workedstill.setText(worked_till3);
        jobprofile.setText(describe_job_profile3);
        notice.setText(notice_period3);
        currentno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (currentno.isChecked()) {
                    workedstill.setText("Present");
                    workedstill.setEnabled(false);
                    workedstill.setInputType(InputType.TYPE_NULL);
                    workedstill.setFocusable(false);
                    notice.setVisibility(View.GONE);
                    chumma.setVisibility(View.GONE);

                }
                if (!currentno.isChecked()) {
                    notice.setVisibility(View.VISIBLE);
                    chumma.setVisibility(View.VISIBLE);
                    workedstill.setText("");
                    workedstill.setEnabled(true);
                    workedstill.setFocusable(true);
                    workedstill.setFocusableInTouchMode(true);
                }
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                designation1=designation.getText().toString();
                organization1=organization.getText().toString();
                workingfrom1=workingfrom.getText().toString();
                workedstill1=workedstill.getText().toString();
                jobprofile1=jobprofile.getText().toString();
                notice1=notice.getText().toString();
                if(id1==null){
                new Work_Experience().execute();}
                else{
                    new Work_Experienceedit().execute();
                }
            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup group, int checkedId)
                                                  {
                                                      radio = (RadioButton) findViewById(checkedId);
                                                      //   Toast.makeText(getBaseContext(), radio.getText(), Toast.LENGTH_SHORT).show();
                                                      current_company=radio.getText().toString();
                                                  }
                                              }
        );


    }



    public  class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            workingfrom.setText("Year: "+view.getYear()+" Month: "+view.getMonth());
        }
    }
    class Work_Experience extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "workinsert";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                //user_id,employment_designation,organization,current_company,started_worked_from,worked_till,describe_job_profile,notice_period
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("employment_designation", designation1));
                nameValuePairs.add(new BasicNameValuePair("organization", organization1));
                nameValuePairs.add(new BasicNameValuePair("current_company", current_company));
                nameValuePairs.add(new BasicNameValuePair("started_worked_from", workingfrom1));
                nameValuePairs.add(new BasicNameValuePair("worked_till", workedstill1));
                nameValuePairs.add(new BasicNameValuePair("describe_job_profile", jobprofile1));
                nameValuePairs.add(new BasicNameValuePair("notice_period", notice1));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");


//                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString(Name, id);

//
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(WorkExperience.this,Message,Toast.LENGTH_SHORT).show();

                            Intent i=new Intent(WorkExperience.this,Profile.class);
                            startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class Work_Experienceedit extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "workupdate";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                //user_id,employment_designation,organization,current_company,started_worked_from,worked_till,describe_job_profile,notice_period
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("employment_designation", designation1));
                nameValuePairs.add(new BasicNameValuePair("organization", organization1));
                nameValuePairs.add(new BasicNameValuePair("current_company", current_company));
                nameValuePairs.add(new BasicNameValuePair("started_worked_from", workingfrom1));
                nameValuePairs.add(new BasicNameValuePair("worked_till", workedstill1));
                nameValuePairs.add(new BasicNameValuePair("describe_job_profile", jobprofile1));
                nameValuePairs.add(new BasicNameValuePair("notice_period", notice1));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");


//                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString(Name, id);

//
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(WorkExperience.this,Message,Toast.LENGTH_SHORT).show();

                            Intent i=new Intent(WorkExperience.this,Profile.class);
                            startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_delete, menu);
         register = menu.findItem(R.id.action_refresh);


        return true;


          }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected


            case R.id.action_refresh:
                new It_skilldelete().execute();

                break;
            // action with ID action_settings was selected

            default:
                break;
        }

        return true;
    }


    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.action_refresh);


        if(edit!=null)
        {
            register.setVisible(true);
        }
        else
        {
            register.setVisible(false);
        }
        return true;
    }

//    class Work_experienceshow extends AsyncTask<String, Void, String> {
//        protected void onPreExecute() {
//            pd.setMessage("loading");
//            pd.show();
//            super.onPreExecute();
//
//
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            HttpClient httpclient = new DefaultHttpClient();
//            String url = Serviceurl.url + "workshow";
//            HttpPost httppost = new HttpPost(url);
//
//
//            try {
//                // Add your data
//
//          execute
//                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//                nameValuePairs.add(new BasicNameValuePair("user_id", "27"));
//
//
//                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());
//
//
//                // Execute HTTP Post Request
//                HttpResponse response = httpclient.execute(httppost);
//                InputStream inputStream = response.getEntity().getContent();
//                InputStreamToStringExample str = new InputStreamToStringExample();
//                String responseServer = str.getStringFromInputStream(inputStream);
//                Log.d("response", "itskillsshowresponse -----" + responseServer);
//
//
//                JSONObject jsonRootObject = new JSONObject(responseServer);
//                JSONArray jsonArray = jsonRootObject.optJSONArray("Work_Experience_details");
//                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
//                    try {
//
//                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
//
//
//                        id1 = jsonObject.getString("id").toString();
//                        employment_designation1= jsonObject.getString("employment_designation").toString();
//                        organization2= jsonObject.getString("organization").toString();
//                        current_company1= jsonObject.getString("current_company").toString();
//                        started_worked_from1= jsonObject.getString("started_worked_from").toString();
//                        worked_till1= jsonObject.getString("worked_till").toString();
//                        describe_job_profile1= jsonObject.getString("describe_job_profile").toString();
//                        notice_period1= jsonObject.getString("notice_period").toString();
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//
//
//
//                                designation.setText(employment_designation1);
//                                organization.setText(organization2);
//                                if(current_company1.equals("Yes")){
//                                    currentyes.setChecked(true);
//                                }
//                                if(current_company1.equals("No")){
//                                    currentno.setChecked(true);
//                                }
//                                workingfrom.setText(started_worked_from1);
//                                workedstill.setText(worked_till1);
//                                jobprofile.setText(describe_job_profile1);
//                                notice.setText(notice_period1);
//
//
//                            }});
//
//                    }
//
//                    catch (Exception e){
//                        e.printStackTrace();
//                    }}
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String aVoid) {
//            super.onPostExecute(aVoid);
//            pd.dismiss();
//        }
//    }
    class It_skilldelete extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "workdelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
           nameValuePairs.add(new BasicNameValuePair("id", id1));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumedeleteresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String message = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {



                        Toast.makeText(WorkExperience.this,message,Toast.LENGTH_SHORT).show();
                        if(message.equals("Deleted successfully")){
                            Intent i=new Intent(WorkExperience.this,Profile.class);
                            startActivity(i);
                        }
                    }});



            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
}

