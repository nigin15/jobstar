package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by user on 9/7/2016.
 */
public class Edit_basicprofile extends AppCompatActivity {
    ProgressDialog pd;
    EditText username, designation, location, experience, phno, email, salary;
    public  static  String username1, designation1, location1, experience1, phno1, email1, salary1, userimage1,
            username2, designation2, location2, experience2, phno2, email2, salary2, userimage2;
    Button save;
    ImageView back;
    CircleImageView camera;
    String picturePath;
    private static final int  RESULT_LOAD_IMAGE=1;
    public final String APP_TAG = "Airstar";
    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 2;

    final int CROP_PIC = 3;
    public String photoFileName = "photo.jpg",user_id;
    Bitmap bitmap;
    Bitmap resizedBitmap;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit);
        pd = new ProgressDialog(Edit_basicprofile.this);
        prefManager = new PrefManager(this);
        username = (EditText) findViewById(R.id.name_edit);
        designation = (EditText) findViewById(R.id.designation_edit);
        location = (EditText) findViewById(R.id.location_edit);
        experience = (EditText) findViewById(R.id.experience_edit);
        phno = (EditText) findViewById(R.id.phoneno_edit);
        email = (EditText) findViewById(R.id.email_edit);
        salary = (EditText) findViewById(R.id.salary_edit);
        save = (Button) findViewById(R.id.save);
        back = (ImageView) findViewById(R.id.back);
        camera = (CircleImageView) findViewById(R.id.home_profile_image);
        try {
            username.setText(username1);
            email.setText(email1);
            phno.setText(phno1);
            designation.setText(designation1);
            location.setText(location1);
            experience.setText(experience1);
            salary.setText(salary1);
        }
        catch (Exception e){
            e.printStackTrace();
        }







        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username2=username.getText().toString().trim();
                designation2=designation.getText().toString().trim();
                location2=location.getText().toString().trim();
                experience2=experience.getText().toString().trim();
                phno2=phno.getText().toString().trim();
                email2=email.getText().toString().trim();
                salary2=salary.getText().toString().trim();


                new Myprofileinsert().execute(username2, designation2, location2, experience2, phno2, email2, salary2);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Edit_basicprofile.this, R.style.AppCompatAlertDialogStyle);

                builder.setTitle("Choose your option");
                builder.setCancelable(false);
                builder.setItems(new CharSequence[]
                                {"Camera", "Gallery", "Cancel"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item
                                switch (which) {
                                    case 0:
                                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri(photoFileName)); // set the image file name
                                        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
                                        // So as long as the result is not null, it's safe to use the intent.
                                        if (intent.resolveActivity(getPackageManager()) != null) {
                                            // Start the image capture intent to take photo
                                            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                                        }
                                        break;
                                    case 1:
                                        Intent i = new Intent(Intent.ACTION_PICK,
                                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(i, RESULT_LOAD_IMAGE);
                                        break;
                                    case 2:
                                        dialog.dismiss();
                                        break;
                                }
                            }
                        });

                final android.support.v7.app.AlertDialog alert = builder.create();
                alert.show();

            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("Request Code"+ requestCode + "Result Code" + resultCode + "data" + data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            performCrop1(selectedImage);
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn,
                    null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            System.out.println("The Fild Path is" + picturePath);
            cursor.close();
            bitmap=StringToBitMap(picturePath);

//            performCrop();
            Glide.with(getApplicationContext()).load(new File(picturePath)).asBitmap().centerCrop().into(new BitmapImageViewTarget(camera) {
            });

        }
        else if (resultCode == RESULT_OK) {
            if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
                // picUri = data.getData();
                performCrop();
            } else if (requestCode == CROP_PIC) {
                Bundle extras = data.getExtras();
                bitmap = extras.getParcelable("data");
                // getResizedBitmap(bitmap,150,100);
                if (bitmap != null) {
                    //Bitmap resized = Bitmap.createScaledBitmap(bitmap, 200, 150, true);
                    camera.setImageBitmap(bitmap);



                }

            }
        }


        else { // Result was a failure
            System.out.println("capture not taken");
        }

    }
    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }

    public Uri getPhotoFileUri(String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            // Get safe storage directory for photos
            // Use `getExternalFilesDir` on Context to access package-specific directories.
            // This way, we don't need to request external read/write runtime permissions.
            File mediaStorageDir = new File(
                    getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
                Log.d(APP_TAG, "failed to create directory");
            }
            // Return the file target for the photo based on filename
            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + fileName));

        }
        return null;
    }
    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }


    private void performCrop1(Uri uri) {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Uri takenPhotoUri = getPhotoFileUri(photoFileName);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(uri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 200);
            cropIntent.putExtra("outputY", 200);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);

            startActivityForResult(cropIntent, CROP_PIC);

        }

        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
    private void performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Uri takenPhotoUri = getPhotoFileUri(photoFileName);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(takenPhotoUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 200);
            cropIntent.putExtra("outputY", 200);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);

            startActivityForResult(cropIntent, CROP_PIC);

        }

        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }}
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    class Myprofileinsert extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "profileupdate";
            HttpPost httppost = new HttpPost(url);
            String image = getStringImage(bitmap);

            try {
                // Add your data


              //  http://www.abservetechdemo.com/projects/client/jobberman/public/mobile/user/profileupdate?user_id=27
                // &designation=halal&location=chennai&experience=1year&email=h@gmail.com&
                // salary=1lacs&phone_number=+12345&avatar=9.jpg&username=gfdngfd

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("username", username2));

                nameValuePairs.add(new BasicNameValuePair("designation", designation2));
                nameValuePairs.add(new BasicNameValuePair("location", location2));
                nameValuePairs.add(new BasicNameValuePair("experience", experience2));
                nameValuePairs.add(new BasicNameValuePair("email", email2));
                nameValuePairs.add(new BasicNameValuePair("salary", salary2));
                nameValuePairs.add(new BasicNameValuePair("phone_number", phno2));
               nameValuePairs.add(new BasicNameValuePair("avatar", image));
                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());


         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);


                final String Message = jsonRootObject.optString("Message");


//                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString(Name, id);

//
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Edit_basicprofile.this,Message,Toast.LENGTH_SHORT).show();
                        if(Message.equals("your profile update successfully")){
                            Intent i=new Intent(Edit_basicprofile.this,Profile.class);
                            startActivity(i);
                        }
                    }});




            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);




        }
    }


}
