package com.abservetech.jobstar;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainPage extends AppCompatActivity {

    ImageView button1;
    Button create, createempl;
    TextView already, signin;
    EditText search;
    ListView mainlist;
    String sendid = "1";
    private List<MainPageGS> mainpagelist = new ArrayList<MainPageGS>();
    MainListAdapter mainListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setContentView(R.layout.activity_main_page);
        search = (EditText) findViewById(R.id.searchedithome);
        button1 = (ImageView) findViewById(R.id.filter);
        create = (Button) findViewById(R.id.create);
        createempl = (Button) findViewById(R.id.createempl);
        signin = (TextView) findViewById(R.id.signin);
        already = (TextView) findViewById(R.id.already);
        mainlist = (ListView) findViewById(R.id.mainlist);
        search.setSelected(false);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchpage = new Intent(MainPage.this, HomeSearch.class);
                startActivity(searchpage);
            }
        });
        createempl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   Intent searchpage = new Intent(MainPage.this, HomePage.class);
                startActivity(searchpage);*/
            }
        });

     /*   new MainpageList().execute();*/
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(MainPage.this, button1);
                popup.getMenuInflater().inflate(R.menu.mainmenu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                                     public boolean onMenuItemClick(MenuItem item) {
                                                         int id = item.getItemId();

                                                         switch (item.getItemId()) {

                                                             case R.id.recent:
                                                                 sendid = "1";
                                                                 new MainpageList().execute(sendid);
                                                                 break;


                                                             case R.id.last:
                                                                 sendid = "2";
                                                                 new MainpageList().execute(sendid);
                                                                 break;

                                                             case R.id.Older:
                                                                 sendid = "3";
                                                                 new MainpageList().execute(sendid);
                                                                 break;
                                                         }
                                                         return true;
                                                     }
                                                 }

                );

                popup.show();
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent seekersignup = new Intent(MainPage.this, SeekSignup.class);
                startActivity(seekersignup);
            }
        });
        already.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent seeklogin = new Intent(MainPage.this, SeekLogin.class);
                startActivity(seeklogin);
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent seeklogin = new Intent(MainPage.this, SeekLogin.class);
                startActivity(seeklogin);
            }
        });
    }

    class MainpageList extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "recent";
            HttpPost httppost = new HttpPost(url);
            String origresponseText = "";

            Intent i = getIntent();

            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", sendid));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);


                //Get the instance of JSONArray that contains JSONObjects
                // postData(responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                JSONArray jsonArray = jsonRootObject.optJSONArray("Recent_Posts");

                mainpagelist.clear();
                if(jsonArray.length()!=0){
                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {
//

                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
//
                        String id = jsonObject.optString("id").toString();
                        String user_id = jsonObject.optString("user_id").toString();
                        String job_title = jsonObject.optString("job_title").toString();
                        String job_desc = jsonObject.optString("job_desc").toString();
                        String created = jsonObject.optString("created").toString();

                        Log.d("response", "profileresponse -----" + id + user_id + job_title + job_desc + created);

                        MainPageGS mainPageGS = new MainPageGS();

                        mainPageGS.setId(id);
                        mainPageGS.setUser_id(user_id);
                        mainPageGS.setJob_title(job_title);
                        mainPageGS.setJob_desc(job_desc);
                        mainPageGS.setCreated(created);

                        mainpagelist.add(mainPageGS);

                        mainListAdapter = new MainListAdapter(MainPage.this, mainpagelist);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mainlist.setAdapter(mainListAdapter);
                                mainListAdapter.notifyDataSetChanged();

                            }
                        });

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
//
//
                }}
                else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainPage.this,"No result found",Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);


        }
    }

    public static class MainListAdapter extends BaseAdapter {
        public static List<MainPageGS> mainpagelist1;
        Context mcontext;
        int globalInc = 0;


        public MainListAdapter(Context mcontext, List<MainPageGS> mainpagelist1) {

            this.mcontext = mcontext;
            this.mainpagelist1 = mainpagelist1;
        }


        @Override
        public int getViewTypeCount() {

            return getCount();
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        public int getCount() {
            return mainpagelist1.size();
        }


        public Object getItem(int position) {
            return mainpagelist1.get(position);
        }


        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                convertView = inflater.inflate(R.layout.mainlist, parent, false);
//                LayoutInflater mInflater = (LayoutInflater) mcontext
//                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//                convertView = mInflater.inflate(R.layout.sosfriendlist, null);

                holder = new ViewHolder();
                holder.jobtitle = (TextView) convertView.findViewById(R.id.jobtitle);
                holder.jobdesc = (TextView) convertView.findViewById(R.id.jobdesc);
                holder.jobtime = (TextView) convertView.findViewById(R.id.jobtime);
                convertView.setTag(holder);


                final MainPageGS m = mainpagelist1.get(position);


                if (m.getJob_title() != null) {
                    if (!m.getJob_title().equals("null")) {

                        holder.jobtitle.setText(m.getJob_title());
                    }
                }

                if (m.getJob_desc() != null) {
                    if (!m.getJob_desc().equals("null")) {

                        holder.jobdesc.setText(m.getJob_desc());
                    }
                }
                if (m.getCreated() != null) {
                    if (!m.getCreated().equals("null")) {

                        holder.jobtime.setText(m.getCreated());
                    }
                }


            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            return convertView;
        }


    }

    static class ViewHolder {

        TextView jobtitle, jobdesc, jobtime;

    }
}
