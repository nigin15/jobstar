package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class OtherDetails extends AppCompatActivity {
    public static String id;
    public static String edit;
    public static String Available_to_join;
    public static String Date_of_birth;
    public static String Gender;
    public static String Home_town;
    public static String Pincode;
    public static String Marital_status;
    public static String Permanent_address;
    EditText dob,hometown,pincode,maritial_satus,address,category,usa_permit,other_permit;

    String dob1,hometown1,pincode1,maritial_satus1,address1,category1,usa_permit1,other_permit1,gender;
    RadioButton radioButton,male,female,yes,no;
    Button save;
    ProgressDialog pd;
    RadioGroup radiogroup;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_details);


        prefManager = new PrefManager(this);

        dob=(EditText)findViewById(R.id.dob);
        hometown=(EditText)findViewById(R.id.hometown);
        pincode=(EditText)findViewById(R.id.pincode);

        maritial_satus=(EditText)findViewById(R.id.maritial_satus);
        address=(EditText)findViewById(R.id.address);
        category=(EditText)findViewById(R.id.category);

        radiogroup=(RadioGroup)findViewById(R.id.radiogroup);
        male=(RadioButton)findViewById(R.id.radioButton);
        female=(RadioButton)findViewById(R.id.radioButton2);
        yes=(RadioButton)findViewById(R.id.radioButton8);
        no=(RadioButton)findViewById(R.id.radioButton9);
        save=(Button)findViewById(R.id.save);
        pd = new ProgressDialog(OtherDetails.this);
        dob.setText(Date_of_birth);
        hometown.setText(Home_town);
        pincode.setText(Pincode);
        maritial_satus.setText(Marital_status);
        address.setText(Permanent_address);
        if(Gender!=null){
            if(Gender.equals("MALE")){
                male.setChecked(true);
            }
            if(Gender.equals("FEMALE")){
                female.setChecked(true);
            }
        }



        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup group, int checkedId)
                                                  {
               radioButton = (RadioButton) findViewById(checkedId);
               gender=radioButton.getText().toString();


                                                  }
                                              }
        );


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dob1=dob.getText().toString();
                hometown1=hometown.getText().toString();
                pincode1=pincode.getText().toString();
                maritial_satus1=maritial_satus.getText().toString();
                address1=address.getText().toString();



                if(id!=null){
                    new otherdetail_edit().execute();
                }
                else{
                new otherdetail_async().execute();}
            }
        });


    }
    class otherdetail_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "personalinsert";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("date_of_birth", dob1));
                nameValuePairs.add(new BasicNameValuePair("gender", gender));
                nameValuePairs.add(new BasicNameValuePair("home_town", hometown1));
                nameValuePairs.add(new BasicNameValuePair("pincode", pincode1));
                nameValuePairs.add(new BasicNameValuePair("marital_status", maritial_satus1));
                nameValuePairs.add(new BasicNameValuePair("permanent_address", address1));
             ;

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");


//                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString(Name, id);

//
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(OtherDetails.this,Message,Toast.LENGTH_SHORT).show();

                            Intent i=new Intent(OtherDetails.this,Profile.class);
                            startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class otherdetail_edit extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "personalupdate";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("date_of_birth", dob1));
                nameValuePairs.add(new BasicNameValuePair("gender", gender));
                nameValuePairs.add(new BasicNameValuePair("home_town", hometown1));
                nameValuePairs.add(new BasicNameValuePair("pincode", pincode1));
                nameValuePairs.add(new BasicNameValuePair("marital_status", maritial_satus1));
                nameValuePairs.add(new BasicNameValuePair("permanent_address", address1));
                ;

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String Message = jsonRootObject.optString("Message");


//                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString(Name, id);

//
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(OtherDetails.this,Message,Toast.LENGTH_SHORT).show();

                            Intent i=new Intent(OtherDetails.this,Profile.class);
                            startActivity(i);

                    }});

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }
    class otherdetail_delete extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "personaldelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", id));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumedeleteresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String message = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(OtherDetails.this,message,Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(OtherDetails.this,Profile.class);
                        startActivity(i);
                    }});


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected



            case R.id.action_refresh:
                new otherdetail_delete().execute();
                break;
            // action with ID action_settings was selected

            default:
                break;
        }

        return true;
    }
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.action_refresh);


        if(edit!=null)
        {
            register.setVisible(true);
        }
        else
        {
            register.setVisible(false);
        }
        return true;
    }}
