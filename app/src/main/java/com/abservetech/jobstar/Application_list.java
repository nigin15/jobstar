package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/22/2016.
 */
public class Application_list extends AppCompatActivity {

    public static String count;
    ImageView back;
    AppliedlistAdapter adapter;
    ListView recommended_listView;
    TextView Chckin;
    public  static   List<HomepageGS> movieList = new ArrayList<HomepageGS>();
    PrefManager prefManager;
    ProgressDialog pd;
    LinearLayout header;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.reccomended_job);
        pd = new ProgressDialog(Application_list.this);
        prefManager = new PrefManager(this);
        header=(LinearLayout)findViewById(R.id.header);
        header.setVisibility(View.GONE);
        back=(ImageView)findViewById(R.id.back);
        recommended_listView=(ListView)findViewById(R.id.recommended_listView);
        Chckin=(TextView)findViewById(R.id.Chckin);
        Chckin.setText("Applied Jobs");
        TextView emptyview=(TextView)findViewById(R.id.emptyview);
     /*   if(count.equals("0")){
            emptyview.setVisibility(View.VISIBLE);
        }
        else{
            emptyview.setVisibility(View.GONE);
        }*/


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        new Apply_async().execute();
    }


    class Apply_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url2 + "setting";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id","27"));

                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());

         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);

                JSONArray jsonArray = jsonRootObject.optJSONArray("Applied_details");

                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {



                        JSONObject jsonObject = jsonArray.getJSONObject(i1);

                        HomepageGS movie=new HomepageGS();

                        movie.setapply1job_title( jsonObject.getString("job_title").toString());
                        movie.setapply1company_name( jsonObject.getString("company_name").toString());
                        movie.setapply1job_experience( jsonObject.getString("job_experience").toString());
                        movie.setapply1job_location( jsonObject.getString("job_location").toString());
                        movie.setapply1job_specialization( jsonObject.getString("keyskills").toString());
                        movie.setapply1job_date( jsonObject.getString("date").toString());
                        movie.setapply1job_id( jsonObject.getString("id").toString());
                       movieList.add(movie);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            adapter=new AppliedlistAdapter(Application_list.this, movieList);
                            recommended_listView.setAdapter(adapter);

                        }
                    });
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            pd.dismiss();


        }
    }

    public class AppliedlistAdapter extends BaseAdapter {

        private List<HomepageGS> movieItems;
        Application_list activity;


        public AppliedlistAdapter(Application_list activity, List<HomepageGS> movieItems) {
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
//        userid = sharedPreferences.getString("userid",null);
//        currency1 = sharedPreferences.getString("currenycode",null);
//        System.out.println("userid in shared preferences in custom search adapter"+userid);

            this.activity = activity;
            this.movieItems = movieItems;
        }

        class ViewHolder {

            protected TextView date,designation,company,experience,location,skill;
            protected CheckBox wishlist;
            ImageView thumbNail;

        }
        @Override
        public int getCount() {
            return movieItems.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                LayoutInflater inflator = LayoutInflater.from(parent.getContext());
                convertView = inflator.inflate(R.layout.job_list1, null);
                viewHolder = new ViewHolder();
                viewHolder. designation=(TextView)convertView.findViewById(R.id.textView5);
                viewHolder. company=(TextView)convertView.findViewById(R.id.textView7);
                viewHolder. experience=(TextView)convertView.findViewById(R.id.textView9);
                viewHolder. location=(TextView)convertView.findViewById(R.id.textView48);
                viewHolder. skill=(TextView)convertView.findViewById(R.id.textView56);
                viewHolder.date=(TextView)convertView.findViewById(R.id.date);

                convertView.setTag(viewHolder);

            }else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final HomepageGS m = movieItems.get(position);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i= new Intent(activity,Job_detail.class);
                    Job_detail.id=m.getapply1job_id();
                    startActivity(i);
              /*      if(!count.equals("0")){
                   }
                    else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                //   Toast.makeText(activity,"Job not applied",Toast.LENGTH_SHORT).show();



                            }
                        });
                    }*/

                }
            });
            if (m.getapply1job_title()!=null)
            {
                if (!m.getapply1job_title().equals("null"))
                {
                    viewHolder. designation.setText(m.getapply1job_title());
                }
            }

            if (m.getapply1job_date()!=null)
            {
                if (!m.getapply1job_date().equals("null"))
                {

                    viewHolder. date.setText(m.getapply1job_date());
                }
            }
            if (m.getapply1company_name()!=null)
            {
                if (!m.getapply1company_name().equals("null"))
                {
                    viewHolder. company.setText(m.getapply1company_name());
                }
            }


            if (m.getapply1job_experience()!=null)
            {
                if (!m.getapply1job_experience().equals("null"))
                {
                    viewHolder. experience.setText(m.getapplyjob_experience());
                }
            }

            if (m.getapply1job_location()!=null)
            {
                if (!m.getapply1job_location().equals("null"))
                {
                    viewHolder. location.setText(m.getapply1job_location());
                }
            }

            if (m.getapply1job_specialization()!=null)
            {
                if (!m.getapply1job_specialization().equals("null"))
                {
                    viewHolder. skill.setText(m.getapply1job_specialization());
                }
            }

            return convertView;
        }
    }
}
