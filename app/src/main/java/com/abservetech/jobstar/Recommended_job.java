package com.abservetech.jobstar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/14/2016.
 */
public class Recommended_job extends Activity{

    public static String count;
    ImageView back;
    RecommendedAdapter adapter;
    ListView recommended_listView;
    public  static   List<HomepageGS> movieList = new ArrayList<HomepageGS>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reccomended_job);

        back=(ImageView)findViewById(R.id.back);
        recommended_listView=(ListView)findViewById(R.id.recommended_listView);
        TextView emptyview=(TextView)findViewById(R.id.emptyview);
        if(count.equals("0")){
            emptyview.setVisibility(View.VISIBLE);
        }
        else{
            emptyview.setVisibility(View.GONE);
        }

        adapter=new RecommendedAdapter(Recommended_job.this, movieList);
        recommended_listView.setAdapter(adapter);



    }




    public class RecommendedAdapter extends BaseAdapter {

        private List<HomepageGS> movieItems;
        Recommended_job activity;


        public RecommendedAdapter(Recommended_job activity, List<HomepageGS> movieItems) {
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
//        userid = sharedPreferences.getString("userid",null);
//        currency1 = sharedPreferences.getString("currenycode",null);
//        System.out.println("userid in shared preferences in custom search adapter"+userid);

            this.activity = activity;
            this.movieItems = movieItems;
        }

         class ViewHolder {

            protected TextView date,designation,company,experience,location,skill;
            protected CheckBox wishlist;
            ImageView thumbNail;

        }
        @Override
        public int getCount() {
            return movieItems.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            Log.d("count---",count);
            ViewHolder viewHolder = null;
            if (convertView == null) {
                LayoutInflater inflator = LayoutInflater.from(parent.getContext());
                convertView = inflator.inflate(R.layout.job_list1, null);
                viewHolder = new ViewHolder();
                viewHolder. designation=(TextView)convertView.findViewById(R.id.textView5);
                viewHolder. company=(TextView)convertView.findViewById(R.id.textView7);
                viewHolder. experience=(TextView)convertView.findViewById(R.id.textView9);
                viewHolder. location=(TextView)convertView.findViewById(R.id.textView48);
                viewHolder. skill=(TextView)convertView.findViewById(R.id.textView56);
                viewHolder. date=(TextView)convertView.findViewById(R.id.date);

                convertView.setTag(viewHolder);

            }else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final HomepageGS m = movieItems.get(position);
            Log.d("job_title---",m.getjob_title());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(!count.equals("0")){
                        Intent i= new Intent(activity,Job_detail.class);
                        Job_detail.id=m.getjob_id();
                        startActivity(i);}
                    else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(activity,"Reccommended job not available now",Toast.LENGTH_SHORT).show();



                            }
                        });
                    }

                }
            });
            if (m.getjob_title()!=null)
            {
                if (!m.getjob_title().equals("null"))
                {
                    viewHolder. designation.setText(m.getjob_title());
                }
            }

            if (m.getjob_date()!=null)
            {
                if (!m.getjob_date().equals("null"))
                {
                    viewHolder. date.setText("Posted on "+m.getjob_date());
                }
            }
            if (m.getcompany_name()!=null)
            {
                if (!m.getcompany_name().equals("null"))
                {
                    viewHolder. company.setText(m.getcompany_name());
                }
            }


            if (m.getjob_experience()!=null)
            {
                if (!m.getjob_experience().equals("null"))
                {
                    viewHolder. experience.setText(m.getjob_experience()+" Years");
                }
            }

            if (m.getjob_location()!=null)
            {
                if (!m.getjob_location().equals("null"))
                {
                    viewHolder. location.setText(m.getjob_location());
                }
            }

            if (m.getjob_specialization()!=null)
            {
                if (!m.getjob_specialization().equals("null"))
                {
                    viewHolder. skill.setText(m.getjob_specialization());
                }
            }

            return convertView;
        }
    }
}
