package com.abservetech.jobstar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/21/2016.
 */
public class Seeker_settings extends AppCompatActivity {
    TextView  application_list,language,notification,about_this,rating,invite,report,signout;
    ProgressDialog pd;
    PrefManager prefManager;
    String url;
    Dialog list_dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.settings);
        prefManager = new PrefManager(this);
        pd = new ProgressDialog(Seeker_settings.this);
        application_list=(TextView)findViewById(R.id.application_list);
        language=(TextView)findViewById(R.id.language);
        notification=(TextView)findViewById(R.id.notification);
        about_this=(TextView)findViewById(R.id.about_this);
        rating=(TextView)findViewById(R.id.rating);
        invite=(TextView)findViewById(R.id.invite);
        report=(TextView)findViewById(R.id.report);
        signout=(TextView)findViewById(R.id.signout);
        Log.e("TAG---", getString(android.R.string.no));

        application_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(Seeker_settings.this,Application_list.class);
                startActivity(i);
            }
        });

        language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent i= new Intent(Seeker_settings.this,LocalizationUpdaterActivity.class);
                startActivity(i);*/
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        list_dialog = new Dialog(Seeker_settings.this);
                        list_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        list_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        list_dialog.setContentView(R.layout.notification);

                       Button save = (Button) list_dialog.findViewById(R.id.save);


                        list_dialog.show();
                                 save.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {



                                list_dialog.dismiss();
                            }
                        });



            }
        });
        about_this.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(Seeker_settings.this,About_this.class);
                startActivity(i);
            }
        });

        rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(Seeker_settings.this,Seeker_rating.class);
                startActivity(i);
            }
        });

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(Seeker_settings.this,Seeker_report.class);
                startActivity(i);
            }
        });

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Seeker_settings.this);
                alertDialogBuilder.setMessage("Do you want to Logout");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        prefManager.logout();

                        Intent intent = new Intent(Seeker_settings.this, MainPage.class);
                        startActivity(intent);

                    }
                });

                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Invite_async().execute();
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                String text="www.jobportal.com/job-listings-urgent openings for";
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>"+url+"</p>"));
                startActivity(Intent.createChooser(sharingIntent,"Share using"));
            }
        });
    }

    class Invite_async extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url2 + "url";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


                //     nameValuePairs.add(new BasicNameValuePair("regid", regid));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPost" + nameValuePairs.toString());

         /* execute */

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);


                Log.d("response", "response -----" + responseServer);

                JSONObject jsonRootObject = new JSONObject(responseServer);

                JSONArray jsonArray = jsonRootObject.optJSONArray("About App");

                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {



                        JSONObject jsonObject = jsonArray.getJSONObject(i1);



                        url=( jsonObject.getString("text").toString());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {



                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);




        }
    }
}
