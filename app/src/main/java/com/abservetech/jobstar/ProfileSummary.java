package com.abservetech.jobstar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ProfileSummary extends AppCompatActivity {

    public static String edit;
    public static String value;
    public static String id;
    Button buttonsummary;
    EditText summaryeedit;
    String summary;
    TextView countleng;
    ProgressDialog pd;
    String summary_headline;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_summary);

        pd = new ProgressDialog(ProfileSummary.this);
        prefManager = new PrefManager(this);
        summaryeedit = (EditText) findViewById(R.id.summaryeedit);
        summaryeedit.setText(value);

        buttonsummary = (Button) findViewById(R.id.buttonsummary);
        countleng = (TextView) findViewById(R.id.profilecount);
        buttonsummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                summary = summaryeedit.getText().toString();

                Intent resume=new Intent(ProfileSummary.this,Profile.class);
                startActivity(resume);

                if(id!=null){
                    new ProfileSummar_edit().execute(summary);
                }
                else{
                    new ProfileSummar().execute(summary);
                }
            }
        });
        summaryeedit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                countleng.setText(String.valueOf(s.length()));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    class ProfileSummar extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "prosuminsert";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("profile_summary", summary));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

        }
    }
    class ProfileSummar_edit extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "prosumupdate";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id",   prefManager.getuserid()));
                nameValuePairs.add(new BasicNameValuePair("profile_summary", summary));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "response -----" + responseServer);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_refresh:
                new ProfileSummardelete().execute();
                break;
            // action with ID action_settings was selected
            case android.R.id.home:
                Intent i= new Intent(ProfileSummary.this,Profile.class);
                startActivity(i);
                return true;
            default:
                break;
        }

        return true;
    }
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        MenuItem register = menu.findItem(R.id.action_refresh);


        if(edit!=null)
        {
            register.setVisible(true);
        }
        else
        {
            register.setVisible(false);
        }
        return true;
    }
    class ProfileSummarshow extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            pd.setMessage("loading");
            pd.show();
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "prosumshow";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_id", "27"));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumeshowresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                JSONArray jsonArray = jsonRootObject.optJSONArray("profile_summary_details");
                for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                    try {

                        JSONObject jsonObject = jsonArray.getJSONObject(i1);
                        id = jsonObject.getString("id").toString();
                        summary_headline= jsonObject.getString("profile_summary").toString();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(summary_headline.equals("null")){
                                }
                                else{
                                    summaryeedit.setText(summary_headline);

                                }
                            }});


                    }

                    catch (Exception e){
                        e.printStackTrace();
                    }}


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
           pd.dismiss();
        }
    }
    class ProfileSummardelete extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setMessage("loading");
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = Serviceurl.url + "prosumdelete";
            HttpPost httppost = new HttpPost(url);


            try {
                // Add your data

         /* execute */
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", id));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.e("mainToPost", "mainToPostprofile" + nameValuePairs.toString());


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                InputStream inputStream = response.getEntity().getContent();
                InputStreamToStringExample str = new InputStreamToStringExample();
                String responseServer = str.getStringFromInputStream(inputStream);
                Log.d("response", "resumedeleteresponse -----" + responseServer);


                JSONObject jsonRootObject = new JSONObject(responseServer);
                final String message = jsonRootObject.optString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ProfileSummary.this,message,Toast.LENGTH_SHORT).show();
                       Intent i= new Intent(ProfileSummary.this,Profile.class);
                        startActivity(i);
                    }});


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
        }
    }
}
